from .errors import BLAuthenticationFailed, BLForbidden, BLNotFound
from mongoengine import MultipleObjectsReturned, DoesNotExist
from .models.permissions import Permissions
from flask import current_app as app, json
from .api_mixin import BLAPIMixin
from flask_jwt_extended import (
    get_jwt,
    get_jti,
    get_jwt_identity,
    verify_jwt_in_request,
    decode_token,
)
from functools import wraps
from bson import ObjectId
from .models import User


def get_current_user():
    """
    Returns current request User instance.
    Raises Bad Token error if user isn't exist.
    """
    user_id = get_jwt_identity()

    try:
        user = User.objects.get(id=ObjectId(user_id))
    except MultipleObjectsReturned:
        raise BLNotFound("user_not_found")

    return user


def add_claims_to_access_token(identity):
    """Add default claims for the Auth token."""
    return {"iss": app.config["JWT_ISSUER"], "aud": identity}


def expired_token_handler(algorithm, data):
    api_mixin = BLAPIMixin()
    return api_mixin.api_error(api_code="authentication_failed")


def unauthorized_loader(token):
    api_mixin = BLAPIMixin()
    return api_mixin.api_error(
        api_code="authentication_failed",
        msg="missing_authorization_header"
    )


def _token_is_revoked():
    token = get_jwt()
    user_id = get_jwt_identity()
    token_storage = TokensStorage(user_id)
    if not token_storage.token_is_in_storage(token["jti"]):
        return True
    return False


def verify_jwt(permissions):
    """
    Verify valid JWT auth token.

    Verify that a valid non-expired JWT token exists in request headers and
    if permissions are given, at least one of the given permissions exist
    within the token.
    """
    verify_jwt_in_request()

    if _token_is_revoked():
        raise BLAuthenticationFailed

    try:
        user = User.objects.get(id=get_jwt_identity())
    except MultipleObjectsReturned:
        raise BLAuthenticationFailed
    except DoesNotExist:
        raise BLAuthenticationFailed

    if permissions:
        for _perm in permissions:
            permission = Permissions.objects(permission=_perm).first()
            if not permission:
                raise BLAuthenticationFailed(f"permission_{_perm}_not_found")

        user_permissions = user.get_permissions()
        for permission in permissions:
            # if user has at least one of the permissions, let him in
            if permission in user_permissions:
                return True

        raise BLForbidden

    return True


def jwt_authenticate(*args):
    """
    JWT auth decorator.

    The decorator ensures that the following assertions are true:
    - used token wasn't expired or revoked
    - user assigned to any of required roles to use the api

    By default it doesn't require from user to have any roles.

    Example::
        @jwt_authenticate('read-product', 'write-product')
        def get_products():  # User must have 'add-product' OR 'write-product' permissions
    """
    wrapped_func = permissions = None
    if args and callable(args[0]):  # used like @deco
        wrapped_func = args[0]
    else:  # used like @deco(role_x, role_y, role_z)
        permissions = args

    def wrapper(fn):
        @wraps(fn)
        def decorator(*a, **k):
            res = verify_jwt(permissions)
            if res is True:
                return fn(*a, **k)
            else:
                return res

        return decorator

    return wrapper if not wrapped_func else wrapper(wrapped_func)


class TokensStorage:
    """
    Tokens container. Provides functionality to add, revoke and list tokens
    """

    REDIS_KEY_PREFIX = "TOKENS"

    def __init__(self, user_id):
        self._user_id = user_id
        self._access_token_ttl = int(
            (app.config["JWT_ACCESS_TOKEN_EXPIRES"] * 1.2).total_seconds()
        )
        self._refresh_token_ttl = int(
            (app.config["JWT_REFRESH_TOKEN_EXPIRES"] * 1.2).total_seconds()
        )
        self._redis = app.extensions["redis"]

    def add(self, tokens):
        """
        Add array of tokens into Redis storage

        :param tokens: List of tokens to store
        """

        app.logger.info(f"generated tokens:\r\n{tokens}")
        pipe = self._redis.pipeline()
        for t in tokens:
            ttl = self._ttl_by_token_type(t)
            token_meta = self._extract_token_meta(t)
            key = self._redis_key(get_jti(t))
            pipe.hmset(key, {k: json.dumps(v) for k, v in token_meta.items()})
            pipe.expire(key, ttl)
        pipe.execute()

    def revoke(self, jtis=None, remove_all=None):
        """
        Revoke ALL or a list of tokens.

        :param jtis: List of token jtis to revoke
        :param remove_all: Flag indicating that all tokens must be removed
        """
        if remove_all:
            jtis = self._get_stored_jtis()

        pipe = self._redis.pipeline()
        for t in jtis:
            pipe.delete(self._redis_key(t))
        pipe.execute()

    def token_is_in_storage(self, jti):
        """
        Check if token with given JTI is in storage (wasn't revoked or expired)

        :param jti: The token JTI
        """

        return bool(self._redis.exists(self._redis_key(jti)))

    def get_all_tokens(self):
        """
        Returns an array of tokens metadata.

        Example of each item in array:
        {
            'exp': 1559409154000,   # expiration time in milli UTC
            'iat': 1556817154000,   # when the token issued in milli UTC
            'jti': '7bcbee45-f668-4c76-99d7-9e8049aaa8e9', # the token id
            'type': 'refresh'  # the token type
        }
        """
        # TODO: Token exp and iat fields seem to contain the same value
        # which is wrong and needs looking into.
        jtis = self._get_stored_jtis()
        tokens = list()
        pipe = self._redis.pipeline()
        for t in jtis:
            pipe.hgetall(self._redis_key(t))

        for raw in pipe.execute():
            if raw is not None:
                token_meta = {
                    k.decode("utf-8"): json.loads(v) for k, v in raw.items()
                }
                tokens.append(token_meta)
        return list(sorted(tokens, key=lambda x: x["iat"]))

    @classmethod
    def from_request_jwt_identity(cls):
        """Return TokensStorage instance for current request user."""
        return cls(get_jwt_identity())

    def _get_stored_jtis(self):
        """Returns an array of user tokens jtis."""
        jtis = list()
        for key in self._redis.scan_iter(self._redis_key("*")):
            jti = key.decode("utf-8").split(":")[-1]
            jtis.append(jti)
        return jtis

    @staticmethod
    def _extract_token_meta(encoded_token):
        """Decode token and extract its meta-data."""
        token_data = decode_token(encoded_token)
        meta = {
            k: v
            for k, v in token_data.items()
            if k in ["jti", "exp", "type", "iat"]
        }
        meta.update(
            {"exp": meta["exp"] * 1000, "iat": meta["iat"] * 1000}
        )  # from unix to milli for UI
        return meta

    def _ttl_by_token_type(self, encoded_token):
        """Return redis key ttl depending on token type."""
        token = decode_token(encoded_token)
        is_refresh = token["type"] == "refresh"
        return (
            self._refresh_token_ttl if is_refresh else self._access_token_ttl
        )

    def _redis_key(self, jti):
        return "{}:{}:{}".format(self.REDIS_KEY_PREFIX, self._user_id, jti)
