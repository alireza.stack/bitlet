
from flask import request, current_app as app
from .errors import BLBadRequest, API_CODES
from .filter_parser import FilterParser
import json


class BLAPIMixin(object):
    _dq_filters_allowed = {}

    def _recursively_extract_fieldnames_from_filter(self, spec) -> set:
        fields = set()
        if "not" in spec:
            fields |= self._recursively_extract_fieldnames_from_filter(
                spec["not"]
            )
        elif "or" in spec:
            for s in spec["or"]:
                fields |= self._recursively_extract_fieldnames_from_filter(s)
        elif "and" in spec or isinstance(spec, list):
            spec = spec["and"] if "and" in spec else spec
            for s in spec:
                fields |= self._recursively_extract_fieldnames_from_filter(s)
        else:
            fields.add(spec["name"])
        return fields

    @property
    def query(self):
        """
        Return search filters from query string.

        :return list: search syntax
        """
        results = []
        filters = request.args.get("query")
        if not filters:
            return results

        try:
            try:
                filters_obj = json.loads(filters)
            except json.JSONDecodeError:
                # probably it is a string? Trying quoting it.
                s_f = '"' + filters.replace('"', '\\"') + '"'
                filters_obj = json.loads(s_f)

            if isinstance(filters_obj, str):
                # If filters is string, it's simple filters.
                p = FilterParser()
                r = p.json_filter_spec(filters_obj)
                results.extend(r)
            else:
                # otherwise it is the proper json style filter spec
                # that needs no further processing
                results.extend(filters_obj)

        except (ValueError, TypeError):
            raise BLBadRequest("Parse error: query: %r" % filters)

        # query_fields = self._recursively_extract_fieldnames_from_filter(
        #     results
        # )
        # non_allowed = query_fields.difference(set(self._dq_filters_allowed))
        # if non_allowed:
        #     raise CGBadRequest(
        #         "Invalid filter. Filtering not allowed for {}".format(
        #             ",".join(non_allowed)
        #         )
        #     )

        return results

    @property
    def pagination_pipeline(self):
        skip = int(request.args.get("skip", 0))
        limit = min(200, int(request.args.get("limit", 50)))
        return [
            {
                "$skip": skip
            },
            {
                "$limit": limit
            }
        ]

    def api_response(  # pylint: disable=R0201,R0913
        self,
        content=None,
        response_code=200,
        content_type="application/json",
        headers=None,
        error_response=False,
        meta=None
    ):
        """
        Return a response with specified code and content.

        :param content: The response content to return
        :param response_code: HTTP response code
        :param content_type: Response content type.
        :param headers: Additional headers for the response.
        :param error_response: Indicate that content needs to be sent in errors
            key of the response instead of data key.
        :param meta: meta information of response like total count, etc
        :return: response
        """
        content = {} if content is None else content
        _resp = {}
        if content_type == "application/json":
            if error_response:
                _resp["errors"] = content
            else:
                if meta:
                    _resp["meta"] = meta

                _resp["data"] = content

        if headers:
            headers.update({"Content-Type": content_type})
        else:
            headers = {"Content-Type": content_type}

        return _resp, response_code, headers

    def api_error(
        self,
        api_code,  # pylint: disable=R0201
        msg=None,
        headers=None,
    ):
        """
        Error response from the API.

        :param api_code: a numeric api error response code
        :param msg: optional message which if not given is fetched from the
            codes dict.
        :param headers: Any additional headers that should be part of response.
        :return: A valid api response.
        """
        api_resp = API_CODES[api_code]
        resp = {
            "code": api_resp['status_code'],
            "message": msg or api_resp['message']
        }
        app.logger.error(f"returned error: {resp}")

        return self.api_response(
            content=resp,
            response_code=api_resp['status_code'],
            headers=headers,
            error_response=True,
        )

    def _make_response(
        self,  # pylint: disable=R0201
        data=None,
        errors=None
    ):
        """
        Create a jsonapi spec 1.0 compatible response.

        Ref: https://jsonapi.org/format/1.0/
        """
        resp = {}
        if data is not None and errors is not None:
            raise BLBadRequest(
                "Cannot have both data and errors inside the same response."
            )

        if data is not None:
            resp["data"] = data
        elif errors is not None:
            resp["errors"] = errors

        return resp
