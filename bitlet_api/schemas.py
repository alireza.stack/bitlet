from bitlet_api.constants import VERIFICATION_STATUS_VERIFIED, VERIFICATION_STATUS_REJECTED, \
    TICKET_PRIORITY_CRITICAL, TICKET_PRIORITY_HIGH, TICKET_PRIORITY_MEDIUM, TICKET_PRIORITY_LOW, \
    TICKET_LOCK, TICKET_CLOSE

account_creation_schema = {
    "title": "UserSignup",
    "description": "User signup.",
    "type": "object",
    "properties": {
        "phone": {"type": "string"},
        "force_code": {"type": "boolean"},
    },
    "required": ["phone"],
    "additionalProperties": False
}

admin_user_patch_verified_schema = {
    "title": "AdminUserPatch",
    "description": "Update user information like first_name, last_name, national_code, etc.",
    "type": "object",
    "properties": {
        "status": {
            "type": "string",
            "enum": [VERIFICATION_STATUS_VERIFIED]
        },
        "first_name": {
            "type": "string",
            "minLength": 3,
        },
        "last_name": {
            "type": "string",
            "minLength": 3,
        },
        "national_code": {
            "type": "string",
            "minLength": 10,
            "maxLength": 10,
            "pattern": "^[0-9]{10}$"
        },
        "dob": {
            "type": "object",
            "properties": {
                "year": {
                    "type": "integer",
                    "minimum": 1300,
                    "maximum": 1400
                },
                "month": {
                    "type": "integer",
                    "minimum": 1,
                    "maximum": 12
                },
                "day": {
                    "type": "integer",
                    "minimum": 1,
                    "maximum": 31
                },
            },
            "required": ["year", "month", "day"],
            "additionalProperties": False
        }
    },
    "required": ["first_name", "last_name", "national_code", "status"],
    "additionalProperties": False
}

admin_user_patch_unverified_schema = {
    "title": "AdminUserPatchUnverified",
    "description": "Update verification document with rejection reason and status.",
    "type": "object",
    "properties": {
        "status": {
            "type": "string",
            "enum": [VERIFICATION_STATUS_REJECTED]
        },
        "rejection_reason": {
            "type": "string"
        },
    },
    "required": ["status", "rejection_reason"],
    "additionalProperties": False
}

add_symbol_schema = {
    "title": "AddSymbolSchema",
    "description": "Add new crypto currency symbols like BTC, ETH, DOGECOIN",
    "type": "object",
    "properties": {
        "en_title": {
            "type": "string"
        },
        "fa_title": {
            "type": "string"
        },
        "symbol": {
            "type": "string"
        },
        "cmc_id": {
            "type": "integer"
        },
        "rank": {
            "type": "integer"
        },
        "description": {
            "type": "string"
        },
        "networks": {
            "description": "list of networks available for crypto transmission",
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "network_type": {
                        "type": "string"
                    },
                    "fee": {
                        "type": "number"
                    },
                    "description": {
                        "type": "string"
                    },
                },
                "required": ["network_type", "fee"],
                "additionalProperties": False
            },
            "minItems": 1,
            "uniqueItems": True
        },
        "logo": {
            "type": "string"
        },
        "is_active": {
            "type": "boolean"
        },
    },
    "required": [
        "en_title", "fa_title", "symbol", "networks", "logo"
    ],
    "additionalProperties": False
}

account_sms_code_type_schema = {
    "title": "SendSMSCode",
    "description": "SMS Code for profile activities like password modification.",
    "type": "object",
    "properties": {
        "code_type": {
            "type": "string",
            "enum": ["password"]
        },
    },
    "required": ["code_type"],
    "additionalProperties": False
}

add_comment_schema = {
    "title": "AddComment",
    "description": "Add comment for symbols by logged-in users",
    "type": "object",
    "properties": {
        "symbol": {
            "type": "string"
        },
        "body": {
            "type": "string"
        }
    },
    "required": ["symbol", "body"],
    "additionalProperties": False
}

account_update_schema = {
    "title": "UserProfileUpdate",
    "description": "User profile update.",
    "type": "object",
    "properties": {
        "signup-password": {"type": "string"},
        "code": {
            "type": "integer",
            "minimum": 1000,
            "maximum": 9999
        },
        "new-password": {"type": "string"},
    },
    "anyOf": [
        {
            "type": "object",
            "required": ["code", "new-password"],
        },
        {
            "type": "object",
            "required": ["signup-password"],
        }
    ],
    "not": {
        "anyOf": [
            {"required": ["signup-password", "code"]},
            {"required": ["signup-password", "new-password"]},
        ]
    },
    "additionalProperties": False
}

verify_bank_card_schema = {
    "title": "VerifyBankCard",
    "description": "Verify User's Bank Card",
    "type": "object",
    "properties": {
        "status": {
            "type": "string",
            "enum": [VERIFICATION_STATUS_VERIFIED]
        },
        "bank_id": {
            "type": "string"
        },
        "account_number": {
            "type": "string",
            "pattern": "^[0-9]{4,}$"
        },
        "sheba": {
            "type": "string",
            "pattern": "^[0-9]{26}$"
        },
    },
    "required": ["status", "bank_id", "account_number", "sheba"],
    "additionalProperties": False
}

reject_bank_card_schema = {
    "title": "RejectBankCard",
    "description": "Reject User's Bank Card",
    "type": "object",
    "properties": {
        "status": {
            "type": "string",
            "enum": [VERIFICATION_STATUS_REJECTED]
        },
        "rejection_reason": {
            "type": "string"
        },
    },
    "required": ["status", "rejection_reason"],
    "additionalProperties": False
}

add_ticket_schema = {
    "title": "BankCards",
    "description": "Add Bank Cards",
    "type": "object",
    "properties": {
        "title": {
            "type": "string"
        },
        "body": {
            "type": "string"
        },
        "attachment": {
            "type": "string"
        },
        "order_id": {
            "type": "string"
        },
        "department_id": {
            "type": "string"
        },
        "priority": {
            "type": "string",
            "enum": [
                TICKET_PRIORITY_CRITICAL,
                TICKET_PRIORITY_HIGH,
                TICKET_PRIORITY_MEDIUM,
                TICKET_PRIORITY_LOW
            ]
        },
    },
    "required": ["title", "body", "department_id"],
    "additionalProperties": False
}

add_ticket_reply_schema = {
    "title": "Add Ticket Reply",
    "description": "Add ticket replies by customer",
    "type": "object",
    "properties": {
        "body": {
            "type": "string"
        },
        "attachment": {
            "type": "string"
        }
    },
    "required": ["body"],
    "additionalProperties": False
}

patch_admin_ticket_schema = {
    "title": "PatchAdminTicket",
    "description": "Update ticket via back office like lock a ticket",
    "type": "object",
    "properties": {
        "command": {
            "type": "string",
            "enum": [TICKET_LOCK]
        },
    },
    "required": ["command"],
    "additionalProperties": False
}

patch_ticket_schema = {
    "title": "PatchTicket",
    "description": "rate/close ticket by customer",
    "type": "object",
    "properties": {
        "command": {
            "type": "string",
            "enum": [TICKET_CLOSE]
        },
        "close": {
            "type": "object",
            "properties": {
                "rate": {
                    "type": "integer",
                    "minimum": 1,
                    "maximum": 5
                },
            },
            "required": ["rate"],
            "additionalProperties": False
        }
    },
    "required": ["command", "close"],
    "additionalProperties": False
}

add_bank_card_schema = {
    "title": "BankCards",
    "description": "Add Bank Cards",
    "type": "object",
    "properties": {
        "account_number": {
            "type": "string",
            "pattern": "^[0-9]{4,}$"
        },
        "sheba": {
            "type": "string",
            "pattern": "^[0-9]{26}$"
        },
        "card_number": {
            "type": "string",
            "pattern": "^[0-9]{16}$"
        },
    },
    "anyOf": [
        {
            "required": ["account_number"],
        },
        {
            "required": ["sheba"],
        }
    ],
    "not": {
        "anyOf": [
            {"required": ["account_number", "sheba"]},
        ]
    },
    "additionalProperties": False
}

role_creation_schema = {
    "title": "AddRole",
    "description": "Add role with a set of permissions.",
    "type": "object",
    "properties": {
        "role": {"type": "string"},
        "permissions": {
            "type": "array",
            "items": {
                "type": "string"
            },
            "uniqueItems": True
        },
        "description": {"type": "string"},
        "is_active": {
            "type": "boolean",
            "default": True
        },
    },
    "required": ["role", "permissions"],
    "additionalProperties": False
}

role_edit_schema = {
    "title": "EditRole",
    "description": "Edit role.",
    "type": "object",
    "properties": {
        "permissions": {
            "type": "array",
            "items": {
                "type": "string"
            },
            "uniqueItems": True
        },
        "description": {"type": "string"},
        "is_deleted": {
            "type": "boolean"
        }
    },
    "anyOf": [
        {
            "required": ["permissions"]
        },
        {
            "required": ["description"]
        },
        {
            "required": ["is_deleted"]
        },
    ],
    "additionalProperties": False
}

login_by_password_schema = {
    "title": "UserLoginByPassword",
    "description": "User login by password.",
    "type": "object",
    "properties": {
        "password": {
            "type": "string",
        },
        "phone": {
            "type": "string"
        }
    },
    "required": ["password", "phone"],
    "additionalProperties": False
}

login_by_code_schema = {
    "title": "UserLogin",
    "description": "User login by code.",
    "type": "object",
    "properties": {
        "code": {
            "type": "integer",
            "minimum": 1000,
            "maximum": 9999
        },
        "phone": {
            "type": "string"
        }
    },
    "required": ["code", "phone"],
    "additionalProperties": False
}
