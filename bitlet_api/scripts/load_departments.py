from flask.cli import with_appcontext
from flask import current_app as app
from ..constants import DEPARTMENTS
from ..models import Department
import click


@click.command(name="load_departments")
@with_appcontext
def load_departments():
    for department in DEPARTMENTS:
        department_qset = Department.objects(fa_name=department["fa_name"]).first()
        if department_qset is None:
            department_obj = Department(**department)
            department_obj.save()
            app.logger.debug(f"department `{department['fa_name']}` added successfully!")

    app.logger.info("departments check are done ;)")
