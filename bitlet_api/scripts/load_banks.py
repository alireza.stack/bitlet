from flask.cli import with_appcontext
from flask import current_app as app
from ..constants import BANKS
from ..models import Bank
import click


@click.command(name="load_banks")
@with_appcontext
def load_banks():
    for bank in BANKS:
        bank_qset = Bank.objects(fa_name=bank["fa_name"]).first()
        if bank_qset is None:
            bank_obj = Bank(**bank)
            bank_obj.save()
            app.logger.debug(f"bank `{bank['fa_name']}` - {bank['icon']} added successfully!")

    app.logger.info("banks check are done ;)")
