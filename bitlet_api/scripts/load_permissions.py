from ..models.permissions import Permissions
from flask.cli import with_appcontext
from flask import current_app as app
import click


# name is used in flask command like -> flask load_permissions
@click.command(name="load_permissions")
@with_appcontext
def load_permissions():
    permissions = [
        "cards",
        "permissions",
        "roles",
        "users",
        "tickets",
        "symbols",
        "uploads",
        "comments"
    ]

    for permission in permissions:
        permission_qset = Permissions.objects(permission=permission).first()
        if permission_qset is None:
            permission_obj = Permissions(permission=permission)
            permission_obj.save()
            app.logger.debug(f"permission `{permission}` added successfully!")

    app.logger.info("permissions check are done ;)")
