from .views.users import UsersView, ProfileView, SMSCodeView, AdminUsersSelfieView, AdminUsersView, AdminUserView
from .views.user_comments import UserCommentsView, AdminCommentsView, CommentsBySymbolView, AdminCommentView
from .views.tickets import TicketsView, TicketView, TicketRepliesView, AdminTicketsView, AdminTicketView
from .views.authentication import TokenByCodeView, RefreshView, TokenByPasswordView
from .views.cards import BankCardsView, AdminBankCardsView, AdminUsersCardsView
from .views.symbols import AdminSymbolsView, SymbolView
from .views.permissions import PermissionsView
from .views.roles import RolesView, RoleView
from .views.uploads import AdminUploadsView
from .views.selfie import SelfieView
from flask_restful import Api
from .utils import limiter


def app_routes(app):
    api = Api(app)

    """Application routes configuration."""
    # use resource_class_kwargs={"app": app} to pass arguments

    # RATE_LIMIT ROUTE CONFIGURATIONS
    TicketView.decorators = [limiter(app).limit(limit_value="2/minute", methods=["PATCH"])]
    AdminUploadsView.decorators = [limiter(app).limit(limit_value="15/minute", methods=["POST"])]
    UserCommentsView.decorators = [limiter(app).limit(limit_value="10/minute", methods=["POST"])]

    # AUTHENTICATION & AUTHORIZATION
    api.add_resource(UsersView, '/users')
    api.add_resource(TokenByCodeView, '/tokens/by-code')
    api.add_resource(TokenByPasswordView, '/tokens/by-password')
    api.add_resource(RefreshView, '/refresh')

    # PROFILE & SETTINGS
    api.add_resource(ProfileView, '/users/self')
    api.add_resource(SMSCodeView, '/users/self/code')
    api.add_resource(BankCardsView, '/users/self/cards')
    api.add_resource(SelfieView, '/users/self/selfie')

    # COMMENTS
    api.add_resource(UserCommentsView, '/users/self/comments')
    api.add_resource(CommentsBySymbolView, '/symbols/<string:symbol_code>/comments')

    # Tickets
    api.add_resource(TicketsView, '/users/self/tickets')
    api.add_resource(TicketView, '/users/self/tickets/<string:ticket_id>')
    api.add_resource(TicketRepliesView, '/users/self/tickets/<string:ticket_id>/replies')

    # ROLES & PERMISSIONS
    api.add_resource(RolesView, '/roles')
    api.add_resource(RoleView, '/roles/<string:role_id>')
    api.add_resource(PermissionsView, '/permissions')

    # SYMBOLS & PRICING
    api.add_resource(SymbolView, '/symbols/<string:symbol_code>')

    # ADMIN ROUTES
    api.add_resource(AdminBankCardsView, '/admin/cards')
    api.add_resource(AdminUploadsView, '/admin/uploads')
    api.add_resource(AdminUsersView, '/admin/users')
    api.add_resource(AdminUserView, '/admin/users/<string:user_id>')
    api.add_resource(AdminUsersSelfieView, '/admin/users/<string:user_id>/selfie')
    api.add_resource(AdminUsersCardsView, '/admin/users/<string:user_id>/cards/<string:card_id>')
    api.add_resource(AdminTicketsView, '/admin/tickets')
    api.add_resource(AdminTicketView, '/admin/tickets/<string:ticket_id>')
    api.add_resource(AdminSymbolsView, '/admin/symbols')
    api.add_resource(AdminCommentsView, '/admin/comments')
    api.add_resource(AdminCommentView, '/admin/comments/<string:comment_id>')
    # api.add_resource(, '/admin/pricing')
