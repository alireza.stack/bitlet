from logging.handlers import RotatingFileHandler
from .celery_app import celery as celery_app
from cg_notification.cgslack import CGSlack
from .constants import BL_LOGGER_CONFIG
from celery.schedules import crontab
from logging import DEBUG, Formatter
from flask import json
import traceback
import requests
import logging
import celery


def create_celery_logger_handler(logger, propagate):
    celery_handler = RotatingFileHandler(
        '/var/log/celery/bitlet-w1.log',
        maxBytes=1024 * 1024 * 200,  # 200MB
        backupCount=10
    )
    celery_formatter = Formatter(BL_LOGGER_CONFIG['formatters']['default']['format'])
    celery_handler.setFormatter(celery_formatter)

    logger.addHandler(celery_handler)
    logger.logLevel = DEBUG
    logger.propagate = propagate


@celery.signals.after_setup_task_logger.connect
def after_setup_celery_task_logger(logger, **kwargs):
    """ This function sets the 'celery.task' logger handler and formatter """
    create_celery_logger_handler(logger, True)


@celery.signals.after_setup_logger.connect
def after_setup_celery_logger(logger, **kwargs):
    """ This function sets the 'celery' logger handler and formatter """
    create_celery_logger_handler(logger, False)


@celery_app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    # Calls latest_pricing() every minute.
    sender.add_periodic_task(60.0, latest_pricing.s(), name='Get latest prices for crypto currencies every minute')

    # Executes every Monday morning at 7:30 a.m.
    # sender.add_periodic_task(
    #     crontab(hour=7, minute=30, day_of_week=1),
    #     dollar_crawler.s(),
    # )


@celery_app.task
def send_slack(message, channel):
    from .runcelery import app

    slack_cfg = json.loads(app.config["SLACK"])
    slack = CGSlack(token=slack_cfg["token"], channel=channel)
    slack.send_message(message)


@celery_app.task
def latest_pricing():
    from .runcelery import app

    slack_cfg = json.loads(app.config["SLACK"])
    slack = CGSlack(token=slack_cfg["token"], channel=slack_cfg["pricing_channel"])

    try:
        logging.info(f"getting crypto currencies latest prices")
        with app.app_context():
            from .models.symbols import Symbols
            from .models.latest_pricing import LatestPricing

            pricing_config = json.loads(app.config["LATEST_PRICING"])

            headers = {
                'Accepts': 'application/json',
                'X-CMC_PRO_API_KEY': pricing_config["CMC_TOKEN"],
            }

            comma_separated_symbols = []
            symbols = (
                Symbols.objects(is_active=True)
                    .only("symbol")
                    .all()
            )
            if not symbols:
                slack.send_message(":x: no symbol found to get its latest pricing!")
                return True

            for symbol in symbols:
                comma_separated_symbols.append(symbol.symbol.lower())

            pricing_url = pricing_config["LATEST_PRICING_URL"] + ",".join(comma_separated_symbols)
            logging.debug(f"getting pricing info for {comma_separated_symbols} symbols...")
            # TODO add retry on request timeout
            latest_pricing_res = requests.get(url=pricing_url, headers=headers, timeout=5)
            if latest_pricing_res.status_code == 200:
                latest_pricing_res = latest_pricing_res.json()
                if latest_pricing_res["status"]["error_code"]:
                    slack.send_message(f":x: CoinMarketCap: {latest_pricing_res['status']}")
                    return False
            else:
                slack.send_message(f":x: latest pricing server error: {latest_pricing_res.status_code}")
                return False

            for symbol, data in latest_pricing_res["data"].items():
                logging.info(f"saving {symbol} latest pricing...")
                latest_pricing_doc = LatestPricing(
                    cmc_id=data["id"],
                    price=data["quote"]["USD"]["price"],
                    volume_24h=data["quote"]["USD"]["volume_24h"],
                    percent_change_1h=data["quote"]["USD"]["percent_change_1h"],
                    percent_change_24h=data["quote"]["USD"]["percent_change_24h"],
                    percent_change_7d=data["quote"]["USD"]["percent_change_7d"],
                    percent_change_30d=data["quote"]["USD"]["percent_change_30d"],
                    percent_change_60d=data["quote"]["USD"]["percent_change_60d"],
                    percent_change_90d=data["quote"]["USD"]["percent_change_90d"]
                )
                latest_pricing_doc.save()

        logging.info("successfully saved latest pricing data")
    except Exception:
        slack.send_message(f":x: latest pricing exception: {traceback.format_exc()}")
        return False
