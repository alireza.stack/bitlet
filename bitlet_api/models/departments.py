from mongoengine import DynamicDocument, signals
from mongoengine.fields import StringField
from ..utils import BaseModel


class Department(DynamicDocument, BaseModel):
    meta = {"collection": "departments"}

    fa_name = StringField(required=True, unique=True)
    description = StringField()


signals.post_save.connect(Department.post_save, sender=Department)
signals.pre_save.connect(Department.pre_save, sender=Department)
signals.pre_save_post_validation.connect(Department.pre_save_post_validation, sender=Department)
