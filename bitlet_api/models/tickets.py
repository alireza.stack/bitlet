from ..constants import USER_TYPES_CUSTOMER, USER_TYPES_AGENT, TICKET_STATUS_WAITING_FOR_REPLY, \
    TICKET_STATUS_REPLIED, TICKET_STATUS_CLOSED, TICKET_PRIORITY_CRITICAL, \
    TICKET_PRIORITY_HIGH, TICKET_PRIORITY_MEDIUM, TICKET_PRIORITY_LOW
from mongoengine import ReferenceField, DynamicDocument, signals
from .departments import Department
from mongoengine.fields import (
    IntField,
    StringField,
    BooleanField,
    DateTimeField
)
from ..utils import BaseModel
from .orders import Order
from .users import User
import mongoengine


class Ticket(DynamicDocument, BaseModel):
    meta = {"collection": "tickets"}

    title = StringField(required=True)
    order = ReferenceField(Order, db_field="order_id", reverse_delete_rule=mongoengine.DENY)
    status = StringField(choices=[TICKET_STATUS_WAITING_FOR_REPLY, TICKET_STATUS_REPLIED, TICKET_STATUS_CLOSED],
                         default=TICKET_STATUS_WAITING_FOR_REPLY)
    department = ReferenceField(Department, db_field="department_id", reverse_delete_rule=mongoengine.DENY)
    user = ReferenceField(User, db_field="user_id", reverse_delete_rule=mongoengine.DENY)
    priority = StringField(
        choices=[TICKET_PRIORITY_CRITICAL, TICKET_PRIORITY_HIGH, TICKET_PRIORITY_MEDIUM, TICKET_PRIORITY_LOW],
        default=TICKET_PRIORITY_HIGH)
    rate = IntField(min_value=1, max_value=5)
    is_locked = BooleanField(default=False)
    locked_by = ReferenceField(User, db_field="locked_by", reverse_delete_rule=mongoengine.DENY)
    locked_at = DateTimeField()


class TicketReplies(DynamicDocument, BaseModel):
    meta = {"collection": "ticket_replies"}

    user = ReferenceField(User, db_field="user_id", reverse_delete_rule=mongoengine.DENY)
    user_type = StringField(choices=[USER_TYPES_CUSTOMER, USER_TYPES_AGENT])
    body = StringField(required=True)
    ticket = ReferenceField(Ticket, db_field="ticket_id", reverse_delete_rule=mongoengine.DENY)
    attachment = StringField(required=False)


signals.post_save.connect(Ticket.post_save, sender=Ticket)
signals.pre_save.connect(Ticket.pre_save, sender=Ticket)
signals.pre_save_post_validation.connect(Ticket.pre_save_post_validation, sender=Ticket)

signals.post_save.connect(TicketReplies.post_save, sender=TicketReplies)
signals.pre_save.connect(TicketReplies.pre_save, sender=TicketReplies)
signals.pre_save_post_validation.connect(TicketReplies.pre_save_post_validation, sender=TicketReplies)
