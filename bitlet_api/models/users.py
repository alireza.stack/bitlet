from ..constants import USER_TYPES_CUSTOMER, USER_AWAITING_APPROVAL, USER_BANNED, USER_INACTIVE, USER_ACTIVE, \
    VERIFICATION_STATUS_WAITING, VERIFICATION_STATUS_VERIFIED, VERIFICATION_STATUS_REJECTED, \
    VERIFICATION_STATUS_UNVERIFIED
from mongoengine import DynamicDocument, signals, ListField, DynamicEmbeddedDocument, \
    EmbeddedDocumentField, ObjectIdField
from flask import current_app as app
from mongoengine.fields import (
    EmailField,
    StringField,
    DateTimeField,
    IntField
)
from ..utils import BaseModel
from .roles import Roles
import bcrypt


class DateOfBirthItem(DynamicEmbeddedDocument):
    year = IntField(min_value=1300, max_value=1400)
    month = IntField(min_value=1, max_value=12)
    day = IntField(min_value=1, max_value=31)


class VerificationItem(DynamicEmbeddedDocument):
    selfie = StringField(
        default=VERIFICATION_STATUS_UNVERIFIED,
        choices=[
            VERIFICATION_STATUS_UNVERIFIED,
            VERIFICATION_STATUS_WAITING,
            VERIFICATION_STATUS_VERIFIED,
            VERIFICATION_STATUS_REJECTED
        ]
    )
    email = StringField(
        default=VERIFICATION_STATUS_UNVERIFIED,
        choices=[
            VERIFICATION_STATUS_UNVERIFIED,
            VERIFICATION_STATUS_WAITING,
            VERIFICATION_STATUS_VERIFIED,
            VERIFICATION_STATUS_REJECTED
        ],
    )
    bank_card = StringField(
        default=VERIFICATION_STATUS_UNVERIFIED,
        choices=[
            VERIFICATION_STATUS_UNVERIFIED,
            VERIFICATION_STATUS_WAITING,
            VERIFICATION_STATUS_VERIFIED,
            VERIFICATION_STATUS_REJECTED
        ]
    )
    phone = StringField(
        default=VERIFICATION_STATUS_UNVERIFIED,
        choices=[
            VERIFICATION_STATUS_UNVERIFIED,
            VERIFICATION_STATUS_WAITING,
            VERIFICATION_STATUS_VERIFIED,
            VERIFICATION_STATUS_REJECTED
        ]
    )


class User(DynamicDocument, BaseModel):
    """Collection with each document containing user information."""

    meta = {"collection": "users"}

    phone = StringField(required=True, unique=True)
    first_name = StringField()
    last_name = StringField()
    national_code = StringField(max_length=10, min_length=10)
    dob = EmbeddedDocumentField(DateOfBirthItem)
    roles = ListField(field=StringField(), default=[USER_TYPES_CUSTOMER], required=True)
    email = EmailField(
        # sparse=True combined with unique=True and required=False
        # means that uniqueness won’t be enforced for None values
        unique=True,
        required=False,
        sparse=True,
    )
    pinned_symbols = ListField(field=ObjectIdField())
    password = StringField()
    last_login = DateTimeField()
    verifications = EmbeddedDocumentField(VerificationItem, default=VerificationItem())
    status = StringField(
        default=USER_AWAITING_APPROVAL,
        choices=[
            USER_AWAITING_APPROVAL,
            USER_ACTIVE,
            USER_BANNED,
            USER_INACTIVE
        ]
    )

    def get_permissions(self):
        # get current user roles and then its underlying permissions
        roles = Roles.objects.filter(role__in=self.roles).all()
        permissions = set()
        for role in roles:
            permissions |= set(role.permissions)

        app.logger.debug(f"list of user permissions: {permissions}")
        return list(permissions)

    def set_password(self, new_password):
        self.password = bcrypt.hashpw(
            password=new_password.encode("utf8"),
            salt=bcrypt.gensalt(12)
        ).decode("utf8")

    def save(self, *args, **kwargs):
        """Convert string to DBRef before saving user_id"""
        if self.status == USER_AWAITING_APPROVAL:
            if self.verifications.selfie == VERIFICATION_STATUS_VERIFIED and \
                    self.verifications.bank_card == VERIFICATION_STATUS_VERIFIED and \
                    self.verifications.phone == VERIFICATION_STATUS_VERIFIED:
                self.status = USER_ACTIVE

        return super(DynamicDocument, self).save(*args, **kwargs)


signals.post_save.connect(User.post_save, sender=User)
signals.pre_save.connect(User.pre_save, sender=User)
signals.pre_save_post_validation.connect(User.pre_save_post_validation, sender=User)
