from mongoengine import DynamicDocument, signals
from mongoengine.fields import StringField
from ..utils import BaseModel


class Permissions(DynamicDocument, BaseModel):
    """Collection with each document containing hard-coded permissions."""

    meta = {"collection": "permissions"}

    permission = StringField(required=True, unique=True)


signals.post_save.connect(Permissions.post_save, sender=Permissions)
signals.pre_save.connect(Permissions.pre_save, sender=Permissions)
signals.pre_save_post_validation.connect(Permissions.pre_save_post_validation, sender=Permissions)
