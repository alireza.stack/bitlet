from mongoengine import DynamicDocument, ListField, signals
from mongoengine.fields import StringField, BooleanField
from ..utils import BaseModel


class Roles(DynamicDocument, BaseModel):
    """Collection with each document containing roles and permissions information."""

    meta = {"collection": "roles"}

    role = StringField(required=True, unique=True)
    permissions = ListField(field=StringField())
    description = StringField()
    is_deleted = BooleanField(default=False)


signals.post_save.connect(Roles.post_save, sender=Roles)
signals.pre_save.connect(Roles.pre_save, sender=Roles)
signals.pre_save_post_validation.connect(Roles.pre_save_post_validation, sender=Roles)
