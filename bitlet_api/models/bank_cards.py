from mongoengine import ReferenceField, DynamicDocument, signals
from ..constants import BANK_CARD_NOT_VERIFIED, \
    BANK_CARD_VERIFIED, BANK_CARD_REJECTED
from mongoengine.fields import (
    StringField,
    BooleanField
)
from ..utils import BaseModel
from .users import User
import mongoengine


class Bank(DynamicDocument, BaseModel):
    meta = {"collection": "banks"}

    fa_name = StringField(required=True, unique=True)
    icon = StringField()


class BankCard(DynamicDocument, BaseModel):
    meta = {"collection": "bank_cards"}

    card_number = StringField()
    sheba = StringField()
    account_number = StringField(required=True)
    bank = ReferenceField(Bank, db_field="bank_id", reverse_delete_rule=mongoengine.DENY)
    user = ReferenceField(User, db_field="user_id", reverse_delete_rule=mongoengine.DENY)
    status = StringField(choices=[BANK_CARD_NOT_VERIFIED, BANK_CARD_VERIFIED, BANK_CARD_REJECTED],
                         default=BANK_CARD_NOT_VERIFIED)
    is_default = BooleanField(default=False)


signals.post_save.connect(Bank.post_save, sender=Bank)
signals.pre_save.connect(Bank.pre_save, sender=Bank)
signals.pre_save_post_validation.connect(Bank.pre_save_post_validation, sender=Bank)

signals.post_save.connect(BankCard.post_save, sender=BankCard)
signals.pre_save.connect(BankCard.pre_save, sender=BankCard)
signals.pre_save_post_validation.connect(BankCard.pre_save_post_validation, sender=BankCard)
