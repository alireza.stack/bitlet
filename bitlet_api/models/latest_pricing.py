from mongoengine import DynamicDocument, FloatField, IntField
from ..utils import BaseModel


class LatestPricing(DynamicDocument, BaseModel):
    meta = {"collection": "latest_pricing"}

    cmc_id = IntField()
    # TODO should we round price?
    price = FloatField(required=True)
    volume_24h = FloatField()
    percent_change_1h = FloatField()
    percent_change_24h = FloatField()
    percent_change_7d = FloatField()
    percent_change_30d = FloatField()
    percent_change_60d = FloatField()
    percent_change_90d = FloatField()
