from ..constants import VERIFICATION_EMAIL, VERIFICATION_SELFIE, VERIFICATION_BANK_CARDS, VERIFICATION_PHONE, \
    VERIFICATION_STATUS_WAITING, VERIFICATION_STATUS_VERIFIED, VERIFICATION_STATUS_REJECTED
from mongoengine import ReferenceField, DynamicDocument, DictField, DateTimeField, signals
from mongoengine.fields import StringField
from ..utils import BaseModel
from .users import User
import mongoengine


class Verification(DynamicDocument, BaseModel):
    meta = {"collection": "verifications"}

    verification_type = StringField(
        choices=[
            VERIFICATION_EMAIL,
            VERIFICATION_SELFIE,
            VERIFICATION_BANK_CARDS,
            VERIFICATION_PHONE
        ],
        required=True
    )
    user = ReferenceField(User, db_field="user_id", reverse_delete_rule=mongoengine.DENY)
    data = DictField(default={})
    status = StringField(
        choices=[
            VERIFICATION_STATUS_WAITING,
            VERIFICATION_STATUS_VERIFIED,
            VERIFICATION_STATUS_REJECTED
        ],
        default=VERIFICATION_STATUS_WAITING
    )
    verified_by = ReferenceField(User, db_field="verified_by", reverse_delete_rule=mongoengine.DENY)
    verified_at = DateTimeField()


signals.post_save.connect(Verification.post_save, sender=Verification)
signals.pre_save.connect(Verification.pre_save, sender=Verification)
signals.pre_save_post_validation.connect(Verification.pre_save_post_validation, sender=Verification)
