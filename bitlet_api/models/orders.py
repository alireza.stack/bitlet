from ..constants import ORDER_BUY, ORDER_SELL, ORDER_STATUS_REJECTED, \
    ORDER_STATUS_SUCCESSFUL, DRAFT_ORDER_DRAFT, DRAFT_ORDER_FAILED
from mongoengine import DynamicDocument, ReferenceField, signals
from mongoengine.fields import (
    StringField,
    FloatField,
    IntField
)
from ..utils import BaseModel
from .payments import Payment
from .symbols import Symbols
from .users import User
import mongoengine


class Order(DynamicDocument, BaseModel):
    meta = {"collection": "orders"}

    user = ReferenceField(User, db_field="user_id", reverse_delete_rule=mongoengine.DENY)
    order_type = StringField(choices=[ORDER_BUY, ORDER_SELL], required=True)
    symbol = ReferenceField(Symbols, db_field="symbol_id", reverse_delete_rule=mongoengine.DENY)
    txid = StringField()
    network = StringField(required=True)
    network_fee = IntField()
    source_wallet = StringField()
    destination_wallet = StringField()
    quantity = FloatField()
    toman_price = IntField()
    dollar_price = IntField()
    dollar_rate = IntField()
    status = StringField(choices=[ORDER_STATUS_REJECTED, ORDER_STATUS_SUCCESSFUL], required=True)
    reference_code = StringField()
    payment = ReferenceField(Payment, db_field="payment_id", reverse_delete_rule=mongoengine.DENY)
    verified_by = ReferenceField(User, db_field="verified_by", reverse_delete_rule=mongoengine.DENY)


class DraftOrder(DynamicDocument, BaseModel):
    meta = {"collection": "draft_orders"}

    user = ReferenceField(User, db_field="user_id")
    order_type = StringField(choices=[ORDER_BUY, ORDER_SELL], required=True)
    symbol = ReferenceField(Symbols, db_field="symbol_id")
    txid = StringField()
    network = StringField(required=True)
    source_wallet = StringField()
    destination_wallet = StringField()
    quantity = FloatField()
    toman_price = IntField()
    network_fee = IntField()
    dollar_price = IntField()
    dollar_rate = IntField()
    status = StringField(choices=[DRAFT_ORDER_DRAFT, DRAFT_ORDER_FAILED], required=True)
    reference_code = StringField()
    payment = ReferenceField(Payment, db_field="payment_id")
    verified_by = ReferenceField(User, db_field="verified_by")


signals.post_save.connect(Order.post_save, sender=Order)
signals.pre_save.connect(Order.pre_save, sender=Order)
signals.pre_save_post_validation.connect(Order.pre_save_post_validation, sender=Order)
