from mongoengine import DynamicDocument, signals, EmbeddedDocumentListField, \
    DynamicEmbeddedDocument, FloatField, BooleanField, IntField, BinaryField
from mongoengine.fields import StringField
from ..utils import BaseModel


class NetworkItem(DynamicEmbeddedDocument):
    network_type = StringField(required=True)
    fee = FloatField(required=True)
    description = StringField()


class Symbols(DynamicDocument, BaseModel):
    meta = {"collection": "symbols"}

    en_title = StringField(required=True, unique=True)
    fa_title = StringField(required=True)
    symbol = StringField(unique=True)  # BTC, ETH
    cmc_id = IntField(
        unique=True,
        sparse=True,
    )  # used for linkage with symbols
    rank = IntField(default=9999)
    description = StringField()
    networks = EmbeddedDocumentListField(NetworkItem, required=True)
    logo = StringField(required=True)
    is_active = BooleanField(default=True)


signals.post_save.connect(Symbols.post_save, sender=Symbols)
signals.pre_save.connect(Symbols.pre_save, sender=Symbols)
signals.pre_save_post_validation.connect(Symbols.pre_save_post_validation, sender=Symbols)
