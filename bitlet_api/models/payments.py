from mongoengine import DynamicDocument, StringField, IntField, ReferenceField, DictField, signals
from ..constants import PENDING, SUCCEEDED, FAILED, ONLINE, REFUND
from bitlet_api.models.users import User
from ..utils import BaseModel
import mongoengine


class Payment(DynamicDocument, BaseModel):
    """Collection with each document containing payments information."""

    meta = {"collection": "payments"}

    payment_type = StringField(choices=[ONLINE, REFUND], required=True)
    amount = IntField(required=True)
    user = ReferenceField(User, db_field="user_id", reverse_delete_rule=mongoengine.DENY)
    status = StringField(choices=[PENDING, SUCCEEDED, FAILED], required=True)
    reason = StringField()
    # store payment gateway information
    data = DictField()


signals.post_save.connect(Payment.post_save, sender=Payment)
signals.pre_save.connect(Payment.pre_save, sender=Payment)
signals.pre_save_post_validation.connect(Payment.pre_save_post_validation, sender=Payment)
