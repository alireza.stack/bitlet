from ..constants import COMMENT_NOT_VERIFIED, COMMENT_VERIFIED, COMMENT_REJECTED
from mongoengine import ReferenceField, DynamicDocument, signals
from mongoengine.fields import StringField
from ..utils import BaseModel
from .symbols import Symbols
from .users import User
import mongoengine


class Comments(DynamicDocument, BaseModel):
    meta = {"collection": "comments"}

    user = ReferenceField(User, db_field="user_id", reverse_delete_rule=mongoengine.DENY)
    symbol = ReferenceField(Symbols, db_field="symbol_id", reverse_delete_rule=mongoengine.DENY)
    body = StringField(required=True)
    status = StringField(
        choices=[
            COMMENT_NOT_VERIFIED,
            COMMENT_VERIFIED,
            COMMENT_REJECTED
        ],
        default=COMMENT_NOT_VERIFIED
    )
    verified_by = ReferenceField(User, db_field="verified_by", reverse_delete_rule=mongoengine.DENY)


signals.post_save.connect(Comments.post_save, sender=Comments)
signals.pre_save.connect(Comments.pre_save, sender=Comments)
signals.pre_save_post_validation.connect(Comments.pre_save_post_validation, sender=Comments)
