from mongoengine import DynamicDocument, StringField, ReferenceField, DictField
from mongoengine.fields import DateTimeField
from datetime import datetime
from .users import User
import mongoengine


class Audit(DynamicDocument):
    """Collection with each document containing audit information."""

    meta = {"collection": "audits"}

    source = StringField(required=True)
    source_id = StringField(default=None, null=True)
    action = StringField(choices=["create", "update", "delete"])
    status = StringField(default="pending")
    user_id = ReferenceField(User, null=True, reverse_delete_rule=mongoengine.DENY)
    old_data = DictField(default={})
    new_data = DictField(default={})
    created_at = DateTimeField(default=datetime.utcnow)
