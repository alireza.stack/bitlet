from .authorization import expired_token_handler, unauthorized_loader
from mongoengine import connect as mongoengine_connect
from .scripts.load_departments import load_departments
from .scripts.load_permissions import load_permissions
from .errors import BLHandledExceptions, BLBadRequest
from flask import Flask, current_app as app
from .scripts.load_banks import load_banks
from flask_jwt_extended import JWTManager
from .constants import BL_LOGGER_CONFIG
from mongoengine import NotUniqueError
from logging.config import dictConfig
from flask_redis import FlaskRedis
from .api_mixin import BLAPIMixin
from bson.errors import InvalidId
from flask_cors import CORS
from bson import ObjectId
from .models import User
import traceback
import datetime
import json


def create_app(settings_override=None, load_routes=True, with_logger=True):
    """Application factory."""
    if with_logger:
        dictConfig(BL_LOGGER_CONFIG)

    app = Flask(__name__, instance_relative_config=True)

    app.logger.debug("starting Bitlet API server...")

    # load configuration
    app.config.from_envvar("BITLET_API_CONFIG_FILE")

    if settings_override:
        app.config.update(settings_override)

    CORS(app, resources=json.loads(app.config["CORS"]))

    mongo_conf = json.loads(app.config["MONGODB"])
    app.logger.debug("connecting to MongoDB...")
    mongoengine_connect(
        **mongo_conf
    )

    # Get a MongoDB document to ping the database connection
    from .models.permissions import Permissions
    app.logger.debug("MongoDB Ping")
    Permissions.objects.first()
    app.logger.debug("MongoDB Pong")

    app.logger.info("connected to MongoDB!")

    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(
        seconds=app.config['JWT_ACCESS_TOKEN_EXPIRES']
    )
    app.config['JWT_REFRESH_TOKEN_EXPIRES'] = datetime.timedelta(
        seconds=app.config['JWT_REFRESH_TOKEN_EXPIRES']
    )

    jwt = jwt_callbacks(app)

    # Using the additional_claims_loader, we can specify a method that will be
    # called when creating access tokens, and add these claims to the said
    # token. This method is passed the identity of who the token is being
    # created for, and must return data that is json serializable
    @jwt.additional_claims_loader
    def add_claims_to_access_token(identity):
        # identity must be a valid userId
        user_payload = json.loads(User.objects(id=ObjectId(identity)).first().to_json())
        return {
            "phone": user_payload["phone"],
            "first_name": user_payload.get("first_name", ""),
            "last_name": user_payload.get("last_name", ""),
            "roles": user_payload["roles"],
            "verifications": user_payload["verifications"],
            "status": user_payload["status"],
        }

    if load_routes:
        from .routes import app_routes
        app_routes(app)

    error_handlers(app)

    # redis will be accessible via self.app.extensions["redis"]
    FlaskRedis(app)

    add_commands_to_flask(app)

    app.logger.info("Bitlet API server started!")
    return app


def add_commands_to_flask(flask_app):
    flask_app.cli.add_command(load_permissions)
    flask_app.cli.add_command(load_banks)
    flask_app.cli.add_command(load_departments)


def api_errors_handler(e: BLHandledExceptions):
    """
    Catches Exception exceptions and return good error api request.
    """
    api = BLAPIMixin()
    return api.api_error(e.code, msg=str(e))


def generic_errors_handler(e: Exception):
    """
    Catches exceptions and return nicely formatted error response
    """
    api = BLAPIMixin()
    api_code = "server_error"
    error_message = None
    if type(e) == NotUniqueError:
        error_message = "generic_duplicate_key"
        api_code = "invalid_request"
    elif type(e) == InvalidId:
        error_message = "invalid_id"
        api_code = "invalid_request"

    app.logger.error(traceback.format_exc())
    return api.api_error(api_code=api_code, msg=error_message)


def method_not_allowed(e):
    api = BLAPIMixin()
    return api.api_error("method_not_allowed")


def error_handlers(app: Flask):
    """
    Register error handlers

    :param app: Flask application instance
    """
    app.register_error_handler(BLHandledExceptions, api_errors_handler)
    app.register_error_handler(Exception, generic_errors_handler)
    app.register_error_handler(405, method_not_allowed)


def jwt_callbacks(app):
    """
    Set up custom behavior for JWT based authentication.

    :return: None
    """

    jwt = JWTManager(app)
    jwt.expired_token_loader(expired_token_handler)
    jwt.unauthorized_loader(unauthorized_loader)
    # jwt.user_claims_loader(add_claims_to_access_token)
    return jwt
