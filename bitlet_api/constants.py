""" All constants off the application reside here """

# REDIS KEYS
REDIS_SMS_CODE = "SMS"

# USER STATUSES
USER_AWAITING_APPROVAL = "awaiting-approval"
USER_ACTIVE = "active"
USER_INACTIVE = "inactive"
USER_BANNED = "banned"

# USER TYPES
USER_TYPES_CUSTOMER = "customer"
USER_TYPES_AGENT = "agent"

# TICKET STATUS
TICKET_STATUS_WAITING_FOR_REPLY = "waiting_for_reply"
TICKET_STATUS_REPLIED = "replied"
TICKET_STATUS_CLOSED = "closed"

# TICKET UPDATE COMMANDS (PATCH)
TICKET_LOCK = "lock"
TICKET_CLOSE = "close"

# TICKET PRIORITY
TICKET_PRIORITY_CRITICAL = "critical"
TICKET_PRIORITY_HIGH = "high"
TICKET_PRIORITY_MEDIUM = "medium"
TICKET_PRIORITY_LOW = "low"

# VERIFICATION TYPES
VERIFICATION_EMAIL = "email"
VERIFICATION_SELFIE = "selfie"
VERIFICATION_BANK_CARDS = "bank_cards"
VERIFICATION_PHONE = "phone"

# VERIFICATION STATUS
VERIFICATION_STATUS_UNVERIFIED = "unverified"
VERIFICATION_STATUS_WAITING = "awaiting-approval"
VERIFICATION_STATUS_VERIFIED = "verified"
VERIFICATION_STATUS_REJECTED = "rejected"

# ORDER TYPE
ORDER_BUY = "buy"
ORDER_SELL = "sell"

# ORDER STATUS
ORDER_STATUS_REJECTED = "rejected"
ORDER_STATUS_SUCCESSFUL = "successful"

# DRAFT ORDER
DRAFT_ORDER_DRAFT = "draft"
DRAFT_ORDER_FAILED = "failed"

# BANK CARD STATUS
BANK_CARD_NOT_VERIFIED = "awaiting-approval"
BANK_CARD_VERIFIED = "verified"
BANK_CARD_REJECTED = "rejected"

# USER COMMENTS
COMMENT_NOT_VERIFIED = "awaiting-approval"
COMMENT_VERIFIED = "verified"
COMMENT_REJECTED = "rejected"

# PAYMENTS
ONLINE = "online"
REFUND = "refund"

# DOCUMENT STATUSES
PENDING = "pending"
SUCCEEDED = "succeeded"
FAILED = "failed"

# RANDOM CODE GENERATION
ILLEGAL_CHARACTERS = ["i", "l", "1", "o", "0", "v"]

# PATCH OPERATIONS
REDUCE = "reduce"

BL_LOGGER_CONFIG = {
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {
        'wsgi': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'default'
        }
    },
    'root': {
        'level': 'DEBUG',
        'handlers': ['wsgi']
    }
}

BANKS = [
    {
        "fa_name": "پارسیان",
        "icon": "parsian.svg"
    },
    {
        "fa_name": "ملی",
        "icon": "melli.svg"
    },
    {
        "fa_name": "سامان",
        "icon": "saman.svg"
    },
    {
        "fa_name": "آینده",
        "icon": "ayandeh.svg"
    },
    {
        "fa_name": "اقتصاد نوین",
        "icon": "iqtesad_novin.svg"
    },
    {
        "fa_name": "انصار",
        "icon": "ansar.svg"
    },
    {
        "fa_name": "ایران زمین",
        "icon": "iran_zamin.svg"
    },
    {
        "fa_name": "پاسارگاد",
        "icon": "pasargad.svg"
    },
    {
        "fa_name": "تجارت",
        "icon": "tejarat.svg"
    },
    {
        "fa_name": "توسعه تعاون",
        "icon": "tose_e_taavon.svg"
    },
    {
        "fa_name": "حکمت ایرانیان",
        "icon": "hekmat_iranian.svg"
    },
    {
        "fa_name": "خاورمیانه",
        "icon": "khavarmiyaneh.svg"
    },
    {
        "fa_name": "دی",
        "icon": "dey.svg"
    },
    {
        "fa_name": "رفاه کارگران",
        "icon": "refah_kargaran.svg"
    },
    {
        "fa_name": "سپه",
        "icon": "sepah.svg"
    },
    {
        "fa_name": "سرمایه",
        "icon": "sarmayeh.svg"
    },
    {
        "fa_name": "سینا",
        "icon": "sina.svg"
    },
    {
        "fa_name": "شهر",
        "icon": "shahr.svg"
    },
    {
        "fa_name": "صادرات ایران",
        "icon": "saderat.svg"
    },
    {
        "fa_name": "صنعت و معدن",
        "icon": "sanat_madan.svg"
    },
    {
        "fa_name": "قرض الحسنه رسالت",
        "icon": "resalat.svg"
    },
    {
        "fa_name": "قرض الحسنه مهر ایران",
        "icon": "mehr.svg"
    },
    {
        "fa_name": "قوامین",
        "icon": "qavamin.svg"
    },
    {
        "fa_name": "کارآفرین",
        "icon": "kar_afarin.svg"
    },
    {
        "fa_name": "کشاورزی",
        "icon": "keshavarzi.svg"
    },
    {
        "fa_name": "گردشگری",
        "icon": "gardeshgari.svg"
    },
    {
        "fa_name": "مسکن",
        "icon": "maskan.svg"
    },
    {
        "fa_name": "ملت",
        "icon": "mellat.svg"
    },
    {
        "fa_name": "مهر اقتصاد",
        "icon": "mehr_iqtesad.svg"
    },
]

DEPARTMENTS = [
    {
        "fa_name": "فنی",
        "description": "گزارش باگ و خطای نرم افزاری"
    },
    {
        "fa_name": "پیگیری خرید، فروش",
        "description": "پیگیری سفارش انجام نشده خرید و یا فروش ارز"
    },
    {
        "fa_name": "انتقادات و پیشنهادات",
        "description": "انتقاد و یا پیشنهادات در مورد روند کاری سازمان، پشتیبانی و دیگر موارد"
    },
]
