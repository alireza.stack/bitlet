from flask_jwt_extended import get_jwt_identity, verify_jwt_in_request, create_access_token, create_refresh_token
from .constants import ILLEGAL_CHARACTERS, USER_INACTIVE, USER_BANNED
from cg_notification.errors import CGInvalidSMSGateway, CGSMSNotSent
from .errors import BLBadRequest, BLAuthenticationFailed
from flask_limiter.util import get_remote_address
from mongoengine.fields import DateTimeField
from flask import current_app as app, json
from cg_notification.sms import SMSSender
from random import choice, randint
from flask_limiter import Limiter
from datetime import datetime
from bson import Decimal128
from mongoengine import Q
from bson import ObjectId
import phonenumbers
import traceback
import string


class BitletBaseModel(object):
    created_at = DateTimeField(default=datetime.utcnow)
    updated_at = DateTimeField(default=datetime.utcnow)


class BaseModel(BitletBaseModel):
    @classmethod
    def post_save(cls, sender, document, **kwargs):
        try:
            verify_jwt_in_request()
            user_id = get_jwt_identity()
        except Exception:
            user_id = None

        if 'created' in kwargs:
            from .models import Audit
            audit_doc = Audit.objects(
                user_id=ObjectId(user_id) if user_id else None,
                source=sender._get_collection_name(),
                action="create" if kwargs['created'] else "update",
                status="pending",
                new_data={}
            ).order_by("-_id").first()
            if not audit_doc:
                audit_doc = Audit(
                    user_id=ObjectId(user_id),
                    source=sender._get_collection_name(),
                    new_data=document.to_mongo(),
                    action="create" if kwargs['created'] else "update",
                )
            else:
                old_set = sorted(audit_doc.old_data.items())
                new_set = sorted(document.to_mongo().items())
                difference = {}
                if not old_set:
                    difference = document.to_mongo()
                else:
                    for key, item in new_set:
                        if key not in ['updated_at'] and (key, item) not in old_set:
                            difference[key] = item

                audit_doc.new_data = difference

            audit_doc.source_id = str(document.id)
            audit_doc.status = "success"
            audit_doc.save()
            app.logger.info(f"AUDIT: document saved successfully {audit_doc.id}")

    @classmethod
    def pre_save(cls, sender, document, **kwargs):
        # update updated_at field in all documents before save (pre_save)
        document.updated_at = datetime.utcnow()

    @classmethod
    def pre_save_post_validation(cls, sender, document, **kwargs):
        from .models import __all__

        try:
            verify_jwt_in_request()
            user_id = get_jwt_identity()
        except Exception:
            user_id = None

        # app.logger.debug(f"AUDIT: target staff: {user_id}")
        # app.logger.debug(f"AUDIT: document data: {document.to_mongo()}")
        # app.logger.debug(f"AUDIT: target collection {sender._get_collection_name()}")
        # app.logger.debug(f"AUDIT: kwargs: {kwargs}")

        old_data = {}
        if document.id:
            for model in __all__:
                if type(document) == model:
                    old_document = model.objects.filter((Q(id=document.id)) | (Q(_id=document.id))).first()
                    if old_document:
                        old_data = old_document.to_mongo()
                    break

        from .models import Audit
        audit = Audit(
            source=sender._get_collection_name(),
            source_id=str(document.id),
            # on user login user_id is not set
            user_id=ObjectId(user_id) if user_id else None,
            action="create" if kwargs['created'] else "update",
            # old_data filled only in UPDATE action
            old_data=old_data
        )
        audit.save()

    @classmethod
    def post_delete(cls, sender, document):
        raise NotImplementedError


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        elif isinstance(o, datetime):
            return o.strftime("%Y-%m-%d %H:%M:%S")
        elif isinstance(o, Decimal128):
            return float(o.__str__())

        return json.JSONEncoder.default(self, o)


def get_json(mongo_doc, excludes=None):
    excludes = excludes or []
    if type(mongo_doc) == dict:
        result = json.loads(
            s=JSONEncoder().encode(o=mongo_doc)
        )
    else:
        result = json.loads(
            s=JSONEncoder().encode(
                o=mongo_doc.to_mongo().to_dict()
            )
        )

    for exclude in excludes:
        if exclude in result:
            del result[exclude]

    return result


def generate_code(
        upper: bool = True,
        numbers: bool = True,
        lower: bool = False,
        punctuation: bool = False,
        length=7):
    seed = ''
    if not any([upper, numbers, lower, punctuation]):
        raise BLBadRequest("code generator needs at least one seed!")

    if upper:
        seed += string.ascii_uppercase
    if lower:
        seed += string.ascii_lowercase
    if punctuation:
        seed += string.punctuation
    if numbers:
        seed += string.digits

    for ch in ILLEGAL_CHARACTERS:
        seed = seed.replace(ch, '')
        seed = seed.replace(ch.upper(), '')

    code = ''
    for _ in range(length):
        code += choice(seed)

    return code


def verify_password_strength(password):
    if len(password) < 6:
        raise BLBadRequest("min_password_length")

    return True


def get_user_by_phone(phone):
    from .models.users import User

    ir_phone = phonenumbers.parse(phone, "IR")
    phone = phonenumbers.format_number(ir_phone, phonenumbers.PhoneNumberFormat.E164)
    user = User.objects.get(phone=phone)
    user_id = str(user.id)
    return user, user_id


def get_user_by_id(user_id):
    from .models.users import User
    user = User.objects(id=ObjectId(user_id)).first()
    return user


def get_tokens(user_id: str):
    from .authorization import TokensStorage

    access_token = create_access_token(identity=user_id)
    refresh_token = create_refresh_token(identity=user_id)
    tokens_storage = TokensStorage(user_id)
    tokens_storage.add(
        tokens=[access_token, refresh_token]
    )

    return access_token, refresh_token


def validate_user_status(user):
    if user.status == USER_INACTIVE:
        app.logger.error(f"user {user.phone} is not active!")
        raise BLAuthenticationFailed("user_inactive")
    elif user.status == USER_BANNED:
        app.logger.error(f"user {user.phone} is banned!")
        raise BLAuthenticationFailed("user_banned")


def send_sms_code(user, redis_key):
    redis = app.extensions["redis"]
    code_ttl = app.config["REDIS_SMS_CODE_TTL"]

    user_login_code = redis_key
    # if key is near its expiration time, generate a new activation code
    if redis.ttl(user_login_code) > 45:
        activation_code = redis.get(user_login_code).decode('ascii')
    else:
        activation_code = randint(1000, 9999)
        redis.set(user_login_code, activation_code, code_ttl)

    app.logger.info(f"activation code: {activation_code}")
    try:
        sms_sender = SMSSender(json.loads(app.config["SMS"]))
        sms_response = sms_sender.send(
            receptor=user.phone,
            token=activation_code
        )
        app.logger.info(f"sms response: {sms_response}")
    except CGInvalidSMSGateway:
        app.logger.error(traceback.format_exc())
        raise BLBadRequest("invalid_sms_gateway")
    except CGSMSNotSent as cg:
        app.logger.info(cg)
        raise BLBadRequest("sms_not_sent")


def limiter(_app):
    _limiter = Limiter(
        _app,
        key_func=get_remote_address
    )
    return _limiter
