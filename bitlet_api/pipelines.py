from .errors import BLBadRequest


class Pipeline(object):
    @staticmethod
    def search_comments(pipeline: list, query: list):
        filter_mapper = {
            "symbol": "symbol.symbol"
        }

        Pipeline.merge_with(
            pipeline=pipeline,
            merge_with="symbols",
            local_field="symbol_id",
            foreign_field="_id",
            alias="symbol",
            unwind=True
        )

        for q in query:
            if 'or' in q:
                _match_query = Pipeline.get_or_query(q, filter_mapper)
            else:
                _match_query = Pipeline.get_generic_query(q, filter_mapper)

            pipeline.append({
                "$match": _match_query
            })

            pipeline.append({
                '$sort': {
                    'created_at': -1
                }
            })

    @staticmethod
    def search_symbols(pipeline: list, query: list):
        filter_mapper = {

        }

        for q in query:
            if 'or' in q:
                _match_query = Pipeline.get_or_query(q, filter_mapper)
            else:
                _match_query = Pipeline.get_generic_query(q, filter_mapper)

            pipeline.append({
                "$match": _match_query
            })

            pipeline.append({
                '$sort': {
                    'rank': 1
                }
            })

    @staticmethod
    def search_users(pipeline: list, query: list):
        filter_mapper = {
            "phone": "phone",
            "name": None,
            "status": "status"
        }

        for q in query:
            if 'or' in q:
                _match_query = Pipeline.get_or_query(q, filter_mapper)
            else:
                if q["name"] == "name":
                    _match_query = {
                        "$or": [{
                            "first_name": {
                                "$regex": f'{q["value"]}',
                                "$options": "i"
                            }
                        }, {
                            "last_name": {
                                "$regex": f'{q["value"]}',
                                "$options": "i"
                            }
                        }]
                    }
                else:
                    _match_query = Pipeline.get_generic_query(q, filter_mapper)

            pipeline.append({
                "$match": _match_query
            })

            pipeline.append({
                '$sort': {
                    'created_at': -1
                }
            })

    @staticmethod
    def search_tickets(pipeline: list, query: list):
        filter_mapper = {
            "title": "title",
            "priority": "priority",
            "phone": "user.phone",
            "status": "status",
            "department": "department.fa_name",
            "name": None,
        }

        Pipeline.merge_with(
            pipeline=pipeline,
            merge_with="users",
            local_field="user_id",
            foreign_field="_id",
            alias="user",
            unwind=True,
        )

        Pipeline.merge_with(
            pipeline=pipeline,
            merge_with="departments",
            local_field="department_id",
            foreign_field="_id",
            alias="department",
            unwind=True,
        )

        for q in query:
            if 'or' in q:
                _match_query = Pipeline.get_or_query(q, filter_mapper)
            else:
                if q["name"] == "name":
                    _match_query = {
                        "$or": [{
                            "user.first_name": {
                                "$regex": f'{q["value"]}',
                                "$options": "i"
                            }
                        }, {
                            "user.last_name": {
                                "$regex": f'{q["value"]}',
                                "$options": "i"
                            }
                        }]
                    }
                else:
                    _match_query = Pipeline.get_generic_query(q, filter_mapper)

            pipeline.append({
                "$match": _match_query
            })

        pipeline.append({
            "$project": {
                "created_at": 1,
                "updated_at": 1,
                "title": 1,
                "status": 1,
                "priority": 1,
                "is_locked": 1,
                "locked_by": 1,
                "locked_at": 1,
                "locked_period": 1,
                "user": {
                    "first_name": "$user.first_name",
                    "last_name": "$user.last_name",
                },
                "department": "$department.fa_name",
            }
        })

        pipeline.append({
            '$sort': {
                '_id': -1
            }
        })

    @staticmethod
    def search_cards(pipeline: list, query: list):
        filter_mapper = {
            "card_number": "card_number",
            "sheba": "sheba",
            "account_number": "account_number",
            "status": "status",
            "phone": "user.phone",
            "name": None,
        }

        Pipeline.merge_with(
            pipeline=pipeline,
            merge_with="users",
            local_field="user_id",
            foreign_field="_id",
            alias="user",
            unwind=True,
        )

        for q in query:
            if 'or' in q:
                _match_query = Pipeline.get_or_query(q, filter_mapper)
            else:
                if q["name"] == "name":
                    _match_query = {
                        "$or": [{
                            "user.first_name": {
                                "$regex": f'{q["value"]}',
                                "$options": "i"
                            }
                        }, {
                            "user.last_name": {
                                "$regex": f'{q["value"]}',
                                "$options": "i"
                            }
                        }]
                    }
                else:
                    _match_query = Pipeline.get_generic_query(q, filter_mapper)

            pipeline.append({
                "$match": _match_query
            })

            pipeline.append({
                '$sort': {
                    'status': 1
                }
            })

    @staticmethod
    def merge_with(
            pipeline: list,
            merge_with: str,
            local_field: str,
            foreign_field: str,
            alias: str = None,
            unwind: bool = False
    ):
        alias = alias or merge_with
        pipeline.append(
            {
                "$lookup": {
                    "from": merge_with,
                    "localField": local_field,
                    "foreignField": foreign_field,
                    "as": alias,
                }
            }
        )
        if unwind:
            pipeline.append({
                "$unwind": {
                    "path": f"${alias}",
                    "preserveNullAndEmptyArrays": True
                }
            })

    @staticmethod
    def get_generic_query(q, filter_mapper):
        if q["name"] not in filter_mapper:
            raise BLBadRequest(f"invalid query given: {q['name']}")

        return {
            filter_mapper[q["name"]]: {
                "$regex": f'{q["value"]}',
                "$options": "i"
            }
        }

    @staticmethod
    def get_or_query(q, filter_mapper):
        _match_query = {"$or": []}
        for _or_query in q['or']:
            if _or_query["name"] not in filter_mapper:
                raise BLBadRequest(f"invalid query given: {q['name']}")

            _match_query['$or'].append({
                filter_mapper[_or_query["name"]]: {
                    "$regex": f'{_or_query["value"]}',
                    "$options": "i"
                }
            })
        return _match_query
