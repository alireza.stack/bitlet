API_CODES = {
    "invalid_request": {
        'status_code': 400,
        'message': 'invalid_request'
    },
    "authentication_failed": {
        "status_code": 401,
        "message": "authentication_failed"
    },
    "method_not_allowed": {
        'status_code': 405,
        'message': 'method_not_allowed'
    },
    "resource_forbidden": {
        "status_code": 403,
        "message": "insufficient_permissions"
    },
    "not_found": {
        "status_code": 404,
        "message": "not_found"
    },
    "server_error": {
        "status_code": 500,
        "message": "server_error"
    }
}


class BLHandledExceptions(Exception):
    """
    Base class of handled exceptions.
    This exceptions instances are turned into good api error when raised.

    Example:

        ```
        raise BLBadRequest("invalid_phone")
        ```

        API would return:
        ```
        {
            'errors': {
                'code': 400,
                'message': 'invalid_phone'
            }
        }
        ```
    """

    status = None
    code = None


class BLBadRequest(BLHandledExceptions):
    code = "invalid_request"
    status = API_CODES[code]['status_code']


class BLMethodNotAllowed(BLHandledExceptions):
    code = "method_not_allowed"
    status = API_CODES[code]['status_code']


class BLNotFound(BLHandledExceptions):
    code = "not_found"
    status = API_CODES[code]['status_code']


class BLAuthenticationFailed(BLHandledExceptions):
    code = "authentication_failed"
    status = API_CODES[code]['status_code']


class BLForbidden(BLHandledExceptions):
    code = "resource_forbidden"
    status = API_CODES[code]['status_code']
