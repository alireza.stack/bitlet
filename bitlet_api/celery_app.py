
from celery import Celery
from flask import json
import os

with open(os.environ["BITLET_API_CONFIG_FILE"]) as f:
    for line in f.readlines():
        if line.startswith("CELERY"):
            CELERY_CONFIG = json.loads(line.split("=")[1].replace("'", "").strip())

celery = Celery(
    main=__name__,
    **CELERY_CONFIG
)
