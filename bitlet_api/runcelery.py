
from .celery_init import init_celery
from .celery_app import celery
from . import create_app

"""
This script is run via:
    export BITLET_API_CONFIG_FILE=/etc/bitlet-api.ini && celery -A bitlet_api.runcelery:celery worker -E
"""

app = create_app(load_routes=False, with_logger=False)
init_celery(app, celery)
