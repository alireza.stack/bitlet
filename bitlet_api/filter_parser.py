"""
Filter parser core module
"""

from dateutil.relativedelta import relativedelta
from datetime import datetime
import pyparsing as pp
import re


pp.ParserElement.enablePackrat()


def recognize_type(t):
    """
    Converts literal to a variable in a native Python type.
    For example 2.17 string becomes 2.17 float
    """
    val = t[0]
    if re.search(r"^\d+$", val):
        val = int(val)
    elif re.search(r"^\d+(\.\d+)*$", val):
        val = float(val)
    return val


class TokenHandler:
    """
    Handle parsed tokens and convert then to valid `jsonspec` filter
    """

    def to_json(self):
        """This method must return corresponding `jsonspec` filter"""


class FieldOperator(TokenHandler):
    """
    Parse "by field" queries. Example: "country:China"
    """

    FIELDS_OP = {
        ":": "==",
        "~": "like",
        ">": ">",
        "<": "<",
        ">=": ">=",
        "<=": "<=",
    }

    def __init__(self, *args):
        _, _, tokens = args
        self.field_name = self.op = self.field_value = self.token = None
        if not isinstance(tokens[0][0], BinaryOperator):
            self.field_name, self.op, self.field_value = tokens
            if isinstance(self.field_value, pp.ParseResults):
                self.field_value = self.field_value[0]
        else:
            self.token = tokens[0][0]

    def to_json(self):
        if not self.token:
            if isinstance(self.field_value, RangeValues):
                return self._handle_range_value()
            elif isinstance(self.field_value, RegexpValues):
                return self._handle_regexp_value()
            else:
                field_value = self.field_value
                if type(field_value) == str and field_value.startswith("now-"):
                    flag_mapper = {
                        "y": "years",
                        "m": "months",
                        "w": "weeks",
                        "d": "days",
                        "h": "hours"
                    }
                    date_interval = self.field_value.split("-").pop()
                    date_interval_flag = re.sub(r'\d', '', date_interval).strip()
                    date_flags = ["y", "m", "w", "d", "h"]
                    if date_interval_flag in date_flags:
                        date_interval_value = int(re.sub(r'[a-zA-Z]', '', date_interval))
                        date_obj = datetime.now() - relativedelta(**{flag_mapper[date_interval_flag]:date_interval_value})
                        field_value = date_obj.strftime("%Y-%m-%d %H:00:00")
                return {
                    "name": self.field_name,
                    "op": self.FIELDS_OP[self.op],
                    "value": field_value,
                }
        else:
            return self.token.to_json()

    def _handle_range_value(self):
        and_ = list()
        if self.field_value.gt_value is not None:
            and_.append(
                {
                    "name": self.field_name,
                    "op": self.field_value.gt_op,
                    "value": self.field_value.gt_value,
                }
            )
        if self.field_value.lt_value is not None:
            and_.append(
                {
                    "name": self.field_name,
                    "op": self.field_value.lt_op,
                    "value": self.field_value.lt_value,
                }
            )
        return {"and": and_}

    def _handle_regexp_value(self):
        return {
            "name": self.field_name,
            "op": "match",
            "value": self.field_value.raw_regexp,
        }


class SpecialFieldValues:
    """Class to handle special field values: ranges, regular expressions etc"""


class RangeValues(SpecialFieldValues):
    """
    Parses and keep params for field:[from TO to] queries
    """

    ASTERISK = "*"

    def __init__(self, *args) -> None:
        self.gt_op = self.lt_op = self.gt_value = self.lt_value = None
        tokens = args[-1]  # e.g ['[', 10, '*', ']']
        self.gt_op = ">=" if tokens[0] == "[" else ">"
        self.lt_op = "<=" if tokens[-1] == "]" else "<"
        self.gt_value = self._value_if_not_asterisk(tokens[1])
        self.lt_value = self._value_if_not_asterisk(tokens[-2])

    def _value_if_not_asterisk(self, val):
        if val != self.ASTERISK:
            return val


class RegexpValues(SpecialFieldValues):
    """
    Parses regexp field values
    """

    BRACKET = "/"  # field:/text/

    def __init__(self, *args):
        self.raw_regexp = args[-1][0]


class BinaryOperator(TokenHandler):
    """
    Parse tokens related to binary operators - OR, AND.
    """

    def __init__(self, s, loc, tokens):  # pylint: disable=W0613
        self.op = tokens[0][1]
        self.tokens = tokens[0][0::2]

    def to_json(self):
        return {self.op.lower(): [t.to_json() for t in self.tokens]}


#
class AndOperator(BinaryOperator):
    """
    Parse tokens related to the AND operator.
    """

    def to_json(self):
        return [t.to_json() for t in self.tokens]


class NotOperator(TokenHandler):
    """
    Parse tokens related to the NOT operator.
    """

    def __init__(self, s, loc, tokens):  # pylint: disable=W0613
        self.token = tokens[0][-1]

    def to_json(self):
        return {"not": self.token.to_json()}


class FilterParser:
    """
    Parse simple filters into a proper json filter syntax.

    Convert filter conditions given in simple format to proper json filter
    format that can be used by code to filter results more easily.

    Example:

        '"ip: 127.0.0.1 status: active"'  # original filter string

        # becomes
        [{"name": "ip", "op": "eq", "value": "127.0.0.1"},
         {"name": "status", "op": "eq", "value": "active"}]
    """

    def __init__(self):
        self.grammar = self.generate_grammar()

    @staticmethod
    def generate_grammar():
        """
        Prepare filter syntax grammar instance.

        Must be called before parsing filters.
        """
        operator = pp.Or(map(pp.Literal, FieldOperator.FIELDS_OP))

        lpar, rpar = map(pp.Suppress, "()")
        lbrack, rbrack = map(pp.Literal, "[]")
        lcbrack, rcbrack = map(pp.Literal, "{}")
        range_lbrack = lbrack | lcbrack
        range_rbrack = rbrack | rcbrack
        asterisk = pp.Literal("*")

        and_, or_, not_, to_ = map(pp.CaselessKeyword, "AND OR NOT TO".split())

        expression = pp.Forward()

        valid_word = pp.Regex(
            r'([ضصثقفغعهخحجچشسیبلاتنمکگظطذدزروژپئa-zA-Z0-9*_+.-]|\\\\|\\([+\-!(){}\[\]^"~*?:]|\|\||&&))+'
        ).setName("word")
        valid_word.setParseAction(recognize_type)
        string = pp.QuotedString('"')
        regexp = pp.QuotedString(RegexpValues.BRACKET).setParseAction(
            RegexpValues
        )
        range_value = valid_word | asterisk | string
        range_ = (
            range_lbrack + range_value + to_ + range_value + range_rbrack
        ).setParseAction(RangeValues)

        term = pp.Forward()
        field_name = valid_word

        string_expr = pp.Group(string) | string | regexp
        word_expr = pp.Group(valid_word) | valid_word
        term << (
            pp.Optional(
                field_name("field") + operator
            )  # pylint: disable=W0106
            + (
                word_expr
                | string_expr
                | pp.Group(lpar + expression + rpar)
                | range_
            )
        ).setParseAction(FieldOperator)

        expression << pp.infixNotation(  # pylint: disable=W0106
            term,
            [
                (
                    (not_ | "!" | "-").setParseAction(lambda: "NOT"),
                    1,
                    pp.opAssoc.RIGHT,
                    NotOperator,
                ),
                (
                    (or_ | "||").setParseAction(lambda: "OR"),
                    2,
                    pp.opAssoc.LEFT,
                    BinaryOperator,
                ),
                (
                    pp.Optional(and_ | "&&").setParseAction(lambda: "AND"),
                    2,
                    pp.opAssoc.LEFT,
                    AndOperator,
                ),
            ],
        )
        return expression

    def json_filter_spec(self, query):
        """Return json spec api style filter spec for given query."""
        res = self.grammar.parseString(query)[0].to_json()
        if isinstance(res, dict):
            res = [res]

        return res

    def parse(self, query):
        """Parse human readable filter and return json filter spec."""
        res = self.json_filter_spec(query)
        # return ParsedFilter(res,
        #                     collection_class=collection_class,
        #                     fields_prefixes=fields_prefixes,
        #                     fields_map=fields_map,
        #                     fields_default_prefix=fields_default_prefix)
        return res
