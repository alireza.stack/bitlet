from jsonschema import validate, ValidationError
from flask_jwt_extended import get_jwt_identity
from flask import current_app as app, request
from ..errors import BLBadRequest, BLNotFound
from ..authorization import jwt_authenticate
from ..schemas import add_comment_schema
from ..constants import COMMENT_VERIFIED
from ..models.comments import Comments
from ..models.symbols import Symbols
from flask_restful import Resource
from ..api_mixin import BLAPIMixin
from ..pipelines import Pipeline
from ..utils import get_json
from bson import ObjectId
import traceback
import flask


class AdminCommentView(Resource, BLAPIMixin):
    @jwt_authenticate("comments")
    def patch(self, comment_id):
        raise NotImplementedError


class AdminCommentsView(Resource, BLAPIMixin):
    @jwt_authenticate("comments")
    def get(self):
        app.logger.debug("getting all comments...")
        query = self.query
        app.logger.info(f"user provided query: {query}")
        pipeline = []
        Pipeline.search_comments(
            pipeline=pipeline,
            query=query
        )

        total_pipe = pipeline + [{"$count": "total"}]
        total_pipe = list(Comments.objects.aggregate(*total_pipe))
        try:
            total_pipe = next(iter(total_pipe))
        except StopIteration:
            return self.api_response(
                content=[],
                meta={
                    "total": 0
                }
            )

        pipeline += self.pagination_pipeline

        app.logger.info(f"final pipeline for comments: {pipeline}")
        comments = list(Comments.objects.aggregate(*pipeline))
        comments = [get_json(comment) for comment in comments]

        return self.api_response(
            content=comments,
            meta={
                "total": total_pipe["total"]
            }
        )


class CommentsBySymbolView(Resource, BLAPIMixin):
    def get(self, symbol_code):
        app.logger.debug(f"getting all public verified comments for symbol {symbol_code}...")
        pipeline = [{
            "$match": {
                "status": COMMENT_VERIFIED
            }
        }]
        Pipeline.search_comments(
            pipeline=pipeline,
            query=[{"name": "symbol", "value": symbol_code}]
        )

        Pipeline.merge_with(
            pipeline=pipeline,
            merge_with="users",
            local_field="user_id",
            foreign_field="_id",
            alias="user",
            unwind=True
        )

        pipeline.append({
            "$project": {
                "created_at": 1,
                "user": {
                    "first_name": "$user.first_name",
                    "last_name": "$user.last_name",
                },
                "body": 1
            }
        })

        total_pipe = pipeline + [{"$count": "total"}]
        total_pipe = list(Comments.objects.aggregate(*total_pipe))
        try:
            total_pipe = next(iter(total_pipe))
        except StopIteration:
            return self.api_response(
                content=[],
                meta={
                    "total": 0
                }
            )

        pipeline += self.pagination_pipeline

        app.logger.info(f"final pipeline for public comments: {pipeline}")
        comments = list(Comments.objects.aggregate(*pipeline))
        comments = [get_json(comment) for comment in comments]

        return self.api_response(
            content=comments,
            meta={
                "total": total_pipe["total"]
            }
        )


class UserCommentsView(Resource, BLAPIMixin):
    @jwt_authenticate()
    def post(self):
        try:
            data = request.get_json(force=True)
            validate(data, add_comment_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        user_id = ObjectId(get_jwt_identity())
        symbol = Symbols.objects(symbol=data["symbol"]).first()
        if not symbol:
            raise BLNotFound("symbol_not_found")

        comment = Comments(
            body=str(flask.Markup.escape(data["body"])),
            symbol=symbol,
            user=user_id
        )
        comment.save()

        return self.api_response(response_code=204)
