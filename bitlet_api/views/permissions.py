
from ..models.permissions import Permissions
from ..authorization import jwt_authenticate
from bitlet_api.utils import get_json
from flask import current_app as app
from flask_restful import Resource
from ..api_mixin import BLAPIMixin


class PermissionsView(Resource, BLAPIMixin):

    @jwt_authenticate("permissions")
    def get(self):
        app.logger.debug(f"getting all permissions...")
        permissions = Permissions.objects.all()
        permissions = [get_json(perm) for perm in permissions]

        return self.api_response(
            content=permissions
        )
