from flask import current_app as app, request, json
from jsonschema import validate, ValidationError
from ..errors import BLBadRequest, BLNotFound
from ..authorization import jwt_authenticate
from ..models.symbols import NetworkItem
from ..schemas import add_symbol_schema
from flask_restful import Resource
from ..api_mixin import BLAPIMixin
from ..workers import send_slack
from ..pipelines import Pipeline
from ..utils import get_json
from ..models import Symbols
import traceback


class SymbolView(Resource, BLAPIMixin):
    def get(self, symbol_code):
        symbol = Symbols.objects(symbol=symbol_code, is_active=True).first()
        if not symbol:
            app.logger.error(f"symbol {symbol_code} not found or is inactive!")
            raise BLNotFound("symbol_not_found")

        return self.api_response(
            content=get_json(symbol)
        )


class AdminSymbolsView(Resource, BLAPIMixin):
    @jwt_authenticate("symbols")
    def get(self):
        app.logger.debug("getting all symbols...")
        query = self.query
        app.logger.info(f"user provided query: {query}")
        pipeline = []
        Pipeline.search_symbols(pipeline=pipeline, query=query)

        total_pipe = pipeline + [{"$count": "total"}]
        total_pipe = list(Symbols.objects.aggregate(*total_pipe))
        try:
            total_pipe = next(iter(total_pipe))
        except StopIteration:
            return self.api_response(
                content=[],
                meta={
                    "total": 0
                }
            )

        pipeline += self.pagination_pipeline

        app.logger.info(f"final pipeline for symbols: {pipeline}")
        symbols = list(Symbols.objects.aggregate(*pipeline))
        symbols = [get_json(symbol) for symbol in symbols]

        return self.api_response(
            content=symbols,
            meta={
                "total": total_pipe["total"]
            }
        )

    @jwt_authenticate("symbols")
    def post(self):
        try:
            data = request.get_json(force=True)
            validate(data, add_symbol_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        networks = []
        for network in data["networks"]:
            networks.append(NetworkItem(**network))

        data["networks"] = networks

        app.logger.debug(f"adding crypto symbol {data['en_title']} - {data['symbol']}...")
        symbol = Symbols(
            **data
        )
        symbol.save()
        add_symbol_message = f":white_check_mark: new crypto {data['en_title']} - {data['symbol']} added successfully"
        app.logger.info(add_symbol_message)

        slack_cfg = json.loads(app.config["SLACK"])
        send_slack.delay(
            message=add_symbol_message,
            channel=slack_cfg["customer_support_channel"]
        )

        return self.api_response(response_code=201)
