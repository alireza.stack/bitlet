from ..constants import VERIFICATION_SELFIE, VERIFICATION_STATUS_WAITING, VERIFICATION_STATUS_VERIFIED
from ..authorization import jwt_authenticate, get_current_user
from PIL import Image as PIL_IMAGE, UnidentifiedImageError
from flask import current_app as app, request, json
from flask_restful import Resource
from ..api_mixin import BLAPIMixin
from ..errors import BLBadRequest
from ..models import Verification
from pathlib import Path
import traceback
import secrets
import time
import os


class SelfieView(Resource, BLAPIMixin):
    def __init__(self):
        self.config = json.loads(app.config["UPLOADS"])

    @jwt_authenticate()
    def post(self):
        app.logger.debug("uploading verification file...")
        user = get_current_user()
        selfie_is_not_verified = Verification.objects(
            verification_type=VERIFICATION_SELFIE,
            status=VERIFICATION_STATUS_WAITING,
            user=user,
        ).count()
        if selfie_is_not_verified:
            app.logger.error(f"selfie document is already uploaded for user {user.id} and is waiting for approval")
            raise BLBadRequest("selfie_pending_already_exists")

        if user.verifications.selfie == VERIFICATION_STATUS_VERIFIED:
            app.logger.error(f"user {user.id} has already verified selfie verification step")
            raise BLBadRequest("selfie_already_verified")

        selfie_file = request.files['files']
        destination_path = self.config["image_upload_path"]
        current_year = time.strftime("%Y")
        current_month = time.strftime("%m")
        current_day = time.strftime("%d")
        relative_directory_path = f"/{current_year}/{current_month}/{current_day}"
        absolute_directory_path = os.path.join(destination_path, relative_directory_path.strip("/"))

        try:
            Path(absolute_directory_path).mkdir(parents=True, exist_ok=True)
        except Exception:
            app.logger.error(f"directory could not be created: {traceback.format_exc()}")
            raise BLBadRequest("invalid_upload_folder")

        try:
            im = PIL_IMAGE.open(selfie_file, formats=["JPEG", "PNG"])
            width, height = im.size
            original_name = selfie_file.filename
            mimetype = im.get_format_mimetype()
            extension = im.format.lower()
        except UnidentifiedImageError:
            app.logger.error("invalid image type for selfie document")
            raise BLBadRequest("invalid_image_type")

        # remove Alpha channel on PNG images (RGBA)
        im = im.convert('RGB')

        image_name = f"{secrets.token_hex(nbytes=16)}.jpg"
        image_full_path = os.path.join(absolute_directory_path, image_name)
        im.save(image_full_path, "JPEG")
        app.logger.info(f"selfie document is saved on disk successfully for user {user.id}")
        app.logger.debug("saving verification document...")
        verification = Verification(
            verification_type=VERIFICATION_SELFIE,
            status=VERIFICATION_STATUS_WAITING,
            user=user,
            data={
                "image_name": image_name,
                "relative_path": relative_directory_path,
                "original_name": original_name,
                "mimetype": mimetype,
                "extension": extension,
                "width": width,
                "height": height,
            }
        )
        verification.save()

        user.verifications.selfie = VERIFICATION_STATUS_WAITING
        user.save()

        app.logger.debug("verification document saved successfully!")

        return self.api_response(response_code=204)
