from PIL import Image as PIL_IMAGE, UnidentifiedImageError
from flask import current_app as app, json, request
from ..authorization import jwt_authenticate
from werkzeug.utils import secure_filename
from bitlet_api.errors import BLBadRequest
from ..api_mixin import BLAPIMixin
from flask_restful import Resource
from random import randint
from pathlib import Path
import traceback
import os


class AdminUploadsView(Resource, BLAPIMixin):
    def __init__(self):
        self.config = json.loads(app.config["UPLOADS"])

    @jwt_authenticate("uploads")
    def post(self):
        image_file = request.files['files']
        upload_type = request.form["type"]
        if upload_type not in ["symbol_logo"]:
            raise BLBadRequest("invalid_upload_type")

        clean_image_filename = secure_filename(image_file.filename)
        absolute_directory_path = self.config[f"{upload_type}_upload_path"]

        try:
            Path(absolute_directory_path).mkdir(parents=True, exist_ok=True)
            image_full_path = os.path.join(absolute_directory_path, clean_image_filename)
        except Exception:
            app.logger.error(f"directory could not be created: {traceback.format_exc()}")
            raise BLBadRequest("upload_path_not_created")

        while Path(image_full_path).exists():
            clean_image_filename = f"{randint(100, 9999)}-{clean_image_filename}"
            image_full_path = os.path.join(absolute_directory_path, clean_image_filename)

        try:
            im = PIL_IMAGE.open(image_file, formats=["JPEG", "PNG", "SVG"])
            extension = im.format.lower()
        except UnidentifiedImageError:
            raise BLBadRequest("PIL_error")

        # remove Alpha channel on PNG images (RGBA)
        # im = im.convert('RGB')

        im.save(image_full_path, extension)
        app.logger.info(f"{upload_type} file uploaded successfully in {image_full_path}")

        return self.api_response(
            content={
                "filename": clean_image_filename,
            },
            response_code=201
        )
