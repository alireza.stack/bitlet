
from ..schemas import role_edit_schema, role_creation_schema
from jsonschema import validate, ValidationError
from ..errors import BLBadRequest, BLNotFound
from flask import current_app as app, request
from ..models.permissions import Permissions
from ..authorization import jwt_authenticate
from flask_restful import Resource
from ..api_mixin import BLAPIMixin
from ..models.roles import Roles
from ..utils import get_json
from bson import ObjectId
from ..models import User
import traceback


class RoleView(Resource, BLAPIMixin):

    @jwt_authenticate("roles")
    def get(self, role_id):
        app.logger.debug(f"getting role {role_id}...")

        role_doc = Roles.objects(id=ObjectId(role_id), is_deleted=False).first()
        if not role_doc:
            raise BLNotFound("role_not_found")

        app.logger.info(f"role info: {role_doc.__dict__}")

        role = get_json(role_doc)

        return self.api_response(
            content=role
        )

    @jwt_authenticate("roles")
    def put(self, role_id):
        try:
            data = request.get_json(force=True)
            validate(data, role_edit_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        app.logger.debug(f"updating role {role_id}...")
        role = Roles.objects(id=ObjectId(role_id), is_deleted=False).first()
        if not role:
            raise BLNotFound(f"role_not_found")

        for perm in data.get("permissions", []):
            p = Permissions.objects(permission=perm).first()
            if not p:
                raise BLNotFound(f"permission_{perm}_not_found")

        for key in data:
            setattr(role, key, data[key])

        role.save()
        app.logger.info(f"role {role_id} has been updated with new values: {data}")

        return self.api_response(response_code=204)

    @jwt_authenticate("roles")
    def delete(self, role_id):
        app.logger.debug(f"deleting role {role_id}...")
        role = Roles.objects(id=ObjectId(role_id), is_deleted=False).first()
        if not role:
            raise BLNotFound(f"role_not_found")

        user_with_role = User.objects(roles=role.role).first()
        if user_with_role:
            raise BLBadRequest(f"role_is_read_only")

        role.is_deleted = True
        role.save()
        app.logger.info(f"role {role_id} is deleted successfully!")

        return self.api_response(response_code=204)


class RolesView(Resource, BLAPIMixin):

    def get(self):
        app.logger.debug(f"getting all roles...")

        roles_doc = Roles.objects(is_deleted=False).all()

        roles = [
            get_json(role) for role in roles_doc
            if role.role not in ["admin", "superadmin"]
        ]

        return self.api_response(content=roles)

    @jwt_authenticate("roles")
    def post(self):
        try:
            data = request.get_json(force=True)
            validate(data, role_creation_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        for perm in data.get("permissions", []):
            p = Permissions.objects(permission=perm).first()
            if not p:
                raise BLNotFound(f"permission_{perm}_not_found")

        app.logger.debug(f"adding new role: {data}")
        data["role"] = data["role"].lower()
        role = Roles(**data)
        role.save()

        app.logger.info(f"role {data['role']} added successfully")
        return self.api_response(response_code=204)
