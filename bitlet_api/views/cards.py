from ..constants import VERIFICATION_BANK_CARDS, BANK_CARD_NOT_VERIFIED, \
    VERIFICATION_STATUS_VERIFIED, VERIFICATION_STATUS_REJECTED, USER_ACTIVE, VERIFICATION_STATUS_WAITING
from ..schemas import add_bank_card_schema, verify_bank_card_schema, reject_bank_card_schema
from ..authorization import jwt_authenticate, get_current_user
from ..models import BankCard, Verification, Bank
from jsonschema import validate, ValidationError
from flask_jwt_extended import get_jwt_identity
from ..errors import BLBadRequest, BLNotFound
from flask import current_app as app, request
from ..utils import get_json, get_user_by_id
from flask_restful import Resource
from ..api_mixin import BLAPIMixin
from ..pipelines import Pipeline
from datetime import datetime
from bson import ObjectId
import traceback


class BankCardsView(Resource, BLAPIMixin):
    @jwt_authenticate()
    def get(self):
        user_id = get_jwt_identity()
        cards = BankCard.objects(user=ObjectId(user_id)).all()
        return self.api_response(content=[get_json(card) for card in cards])

    @jwt_authenticate()
    def post(self):
        try:
            data = request.get_json(force=True)
            validate(data, add_bank_card_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        bank_card_is_not_verified = BankCard.objects(
            account_number=data["account_number"],
            status=BANK_CARD_NOT_VERIFIED
        ).count()
        if bank_card_is_not_verified:
            raise BLBadRequest("bank_card_pending_already_exists")

        user = get_current_user()
        bank_card = BankCard(**data)
        bank_card.user = user
        bank_card.save()

        verification = Verification(
            verification_type=VERIFICATION_BANK_CARDS,
            user=ObjectId(get_jwt_identity()),
            data={"bank_card_id": bank_card.id}
        )
        verification.save()

        user.verifications.bank_card = VERIFICATION_STATUS_WAITING
        user.save()

        return self.api_response(response_code=201)


class AdminUsersCardsView(Resource, BLAPIMixin):
    @jwt_authenticate("cards")
    def patch(self, user_id, card_id):
        data = request.get_json(force=True)
        if data["status"] == VERIFICATION_STATUS_VERIFIED:
            try:
                validate(data, verify_bank_card_schema)
                verified = True
            except ValidationError as e:
                app.logger.error(traceback.format_exc())
                raise BLBadRequest("invalid_schema")
        elif data["status"] == VERIFICATION_STATUS_REJECTED:
            try:
                validate(data, reject_bank_card_schema)
                verified = False
            except ValidationError as e:
                app.logger.error(traceback.format_exc())
                raise BLBadRequest("invalid_schema")
        else:
            raise BLBadRequest("invalid_verification_status")

        user = get_user_by_id(user_id)
        if user is None:
            raise BLNotFound("user_not_found")

        if not user.verifications.selfie:
            raise BLBadRequest("selfie_verification_not_passed")

        bank_card = BankCard.objects(
            id=ObjectId(card_id),
            user=user,
        ).first()
        if bank_card is None:
            raise BLNotFound("bank_card_not_found")

        if bank_card.status == VERIFICATION_STATUS_VERIFIED:
            raise BLBadRequest("bank_card_already_verified")
        elif bank_card.status == VERIFICATION_STATUS_REJECTED:
            raise BLBadRequest("bank_card_already_rejected")

        verification = Verification.objects(
            verification_type=VERIFICATION_BANK_CARDS,
            user=user,
            data__bank_card_id=ObjectId(card_id)
        ).first()
        if not verification:
            raise BLNotFound("verification_doc_not_found")

        verification.status = data["status"]
        if not verified:
            verification.data.update({
                "reason": data["rejection_reason"]
            })
            verification.verified_by = get_current_user()
            verification.verified_at = datetime.utcnow()
            verification.save()

            bank_card.status = VERIFICATION_STATUS_REJECTED
            bank_card.save()

            user.verifications.bank_card = VERIFICATION_STATUS_REJECTED
            user.save()

            return self.api_response(response_code=204)

        bank = Bank.objects(id=ObjectId(data["bank_id"])).first()
        if bank is None:
            raise BLNotFound("bank_not_found")

        verification.verified_by = get_current_user()
        verification.verified_at = datetime.utcnow()
        verification.save()

        bank_card.status = VERIFICATION_STATUS_VERIFIED
        bank_card.bank = bank
        bank_card.account_number = data["account_number"]
        bank_card.sheba = data["sheba"]
        bank_card.save()

        user.verifications.bank_card = VERIFICATION_STATUS_VERIFIED
        if user.verifications.selfie == VERIFICATION_STATUS_VERIFIED \
                and user.verifications.phone == VERIFICATION_STATUS_VERIFIED:
            user.status = USER_ACTIVE

        user.save()

        return self.api_response(response_code=204)


class AdminBankCardsView(Resource, BLAPIMixin):
    @jwt_authenticate("cards")
    def get(self):
        app.logger.debug("getting all bank cards...")
        query = self.query
        app.logger.info(f"user provided query: {query}")
        pipeline = []
        Pipeline.search_cards(pipeline=pipeline, query=query)

        total_pipe = pipeline + [{"$count": "total"}]
        total_pipe = list(BankCard.objects.aggregate(*total_pipe))
        try:
            total_pipe = next(iter(total_pipe))
        except StopIteration:
            return self.api_response(
                content=[],
                meta={
                    "total": 0
                }
            )

        pipeline += self.pagination_pipeline

        app.logger.info(f"final pipeline for bank cards: {pipeline}")
        bank_cards = list(BankCard.objects.aggregate(*pipeline))
        bank_cards = [get_json(bank_card) for bank_card in bank_cards]

        return self.api_response(
            content=bank_cards,
            meta={
                "total": total_pipe["total"]
            }
        )
