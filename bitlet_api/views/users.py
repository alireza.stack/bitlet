from ..constants import REDIS_SMS_CODE, VERIFICATION_STATUS_VERIFIED, \
    VERIFICATION_SELFIE, VERIFICATION_STATUS_WAITING, USER_ACTIVE, VERIFICATION_STATUS_REJECTED
from ..schemas import account_creation_schema, account_update_schema, account_sms_code_type_schema, \
    admin_user_patch_verified_schema, admin_user_patch_unverified_schema
from ..models.users import User, VerificationItem, DateOfBirthItem
from ..authorization import jwt_authenticate, get_current_user
from flask import current_app as app, request, json, send_file
from ..utils import get_json, verify_password_strength, \
    get_user_by_phone, send_sms_code, get_user_by_id
from jsonschema import validate, ValidationError
from ..errors import BLBadRequest, BLNotFound
from mongoengine import DoesNotExist
from flask_restful import Resource
from ..api_mixin import BLAPIMixin
from ..models import Verification
from ..pipelines import Pipeline
from datetime import datetime
from bson import ObjectId
from pathlib import Path
import phonenumbers
import traceback
import os


class SMSCodeView(Resource, BLAPIMixin):
    @jwt_authenticate()
    def post(self):
        try:
            data = request.get_json(force=True)
            validate(data, account_sms_code_type_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        user = get_current_user()
        if data["code_type"] == "password":
            redis_key = f"{REDIS_SMS_CODE}:PASSWORD-CHANGE:{user.id}"
        else:
            raise BLBadRequest("invalid_code_type")

        send_sms_code(user, redis_key)

        return self.api_response(response_code=204)


class ProfileView(Resource, BLAPIMixin):
    def __init__(self):
        self.redis = app.extensions["redis"]

    @jwt_authenticate()
    def get(self):
        user = get_current_user()
        user = get_json(user)
        app.logger.info(f"profile info: {user}")
        return self.api_response(content=user)

    @jwt_authenticate()
    def patch(self):
        try:
            data = request.get_json(force=True)
            validate(data, account_update_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        user = get_current_user()
        if "signup-password" in data:
            if user.password:
                raise BLBadRequest("password_already_set")

            verify_password_strength(data["signup-password"])
            user.set_password(data["signup-password"])
            user.save()
        elif "new-password" in data:
            app.logger.debug(f"changing user password for user {user.id}...")
            verify_password_strength(data["new-password"])

            user_login_code = f"{REDIS_SMS_CODE}:PASSWORD-CHANGE:{user.id}"
            activation_code = self.redis.get(user_login_code)
            if not activation_code or data["code"] != int(activation_code):
                app.logger.error(f"user {user.phone} has entered an invalid sms code for password modification!")
                raise BLBadRequest("invalid_sms_code")

            user.set_password(data["new-password"])
            user.save()
            app.logger.info(f"new password has been set for user {user.id}")

        return self.api_response(response_code=204)


class AdminUserView(Resource, BLAPIMixin):
    @jwt_authenticate("users")
    def get(self, user_id):
        user = (
            User.objects(id=ObjectId(user_id))
                .exclude("password")
                .first()
        )
        if not user:
            raise BLNotFound("user_not_found")

        return self.api_response(
            content=get_json(user)
        )


class AdminUsersView(Resource, BLAPIMixin):
    @jwt_authenticate("users")
    def get(self):
        app.logger.debug("getting all users...")
        query = self.query
        app.logger.info(f"user provided query: {query}")
        pipeline = []
        Pipeline.search_users(pipeline=pipeline, query=query)

        total_pipe = pipeline + [{"$count": "total"}]
        total_pipe = list(User.objects.aggregate(*total_pipe))
        try:
            total_pipe = next(iter(total_pipe))
        except StopIteration:
            return self.api_response(
                content=[],
                meta={
                    "total": 0
                }
            )

        pipeline += self.pagination_pipeline

        app.logger.info(f"final pipeline for users: {pipeline}")
        users = list(User.objects.aggregate(*pipeline))
        users = [get_json(user) for user in users]

        return self.api_response(
            content=users,
            meta={
                "total": total_pipe["total"]
            }
        )


class UsersView(Resource, BLAPIMixin):
    def __init__(self):
        self.redis = app.extensions["redis"]
        self.code_ttl = app.config["REDIS_SMS_CODE_TTL"]

    def post(self):
        new_user = False
        force_code = False

        try:
            data = request.get_json(force=True)
            validate(data, account_creation_schema)
            ir_phone = phonenumbers.parse(data["phone"], "IR")
            data["phone"] = phonenumbers.format_number(ir_phone, phonenumbers.PhoneNumberFormat.E164)
            user, user_id = get_user_by_phone(data["phone"])
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")
        except phonenumbers.NumberParseException:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_phone")
        except DoesNotExist:
            new_user = True
            app.logger.info(f"user with phone {data['phone']} does not exist, "
                            f"create a user and generate an activation code")

            if "force_code" in data:
                force_code = data.pop("force_code")

            user_obj = User(**data)
            user = user_obj.save(validate=False)
            user_id = str(user.id)
            app.logger.info(f"user is created with id {user_id}")

        # force_code: user has password but willing to login with sms code
        if not user.password or force_code:
            user_login_code = f"{REDIS_SMS_CODE}:{user_id}"
            send_sms_code(user, user_login_code)

        return self.api_response(content={"is_code": not user.password, "new_user": new_user})


class AdminUsersSelfieView(Resource, BLAPIMixin):
    def __init__(self):
        self.config = json.loads(app.config["UPLOADS"])

    @jwt_authenticate("users")
    def get(self, user_id):
        app.logger.debug(f"getting selfie document for {user_id}...")
        verification = Verification.objects(
            user=ObjectId(user_id),
            verification_type=VERIFICATION_SELFIE,
        ).first()

        destination_path = self.config["image_upload_path"]
        full_path = os.path.join(destination_path, verification.data["relative_path"].strip("/"))
        absolute_image_path = os.path.join(full_path, verification.data["image_name"])

        try:
            Path(full_path).mkdir(parents=True, exist_ok=True)
        except Exception:
            app.logger.error(f"directory could not be created: {traceback.format_exc()}")
            return send_file(os.path.join(destination_path, "error.jpg"))

        if not os.path.exists(absolute_image_path):
            app.logger.error(f"selfie image not found in {absolute_image_path}")
            return send_file(os.path.join(destination_path, "error.jpg"))

        app.logger.info(f"returning selfie image with path: {full_path}")
        return send_file(path_or_file=absolute_image_path)

    @jwt_authenticate("users")
    def patch(self, user_id):
        data = request.get_json(force=True)
        if data["status"] == VERIFICATION_STATUS_VERIFIED:
            try:
                validate(data, admin_user_patch_verified_schema)
                verified = True
            except ValidationError as e:
                app.logger.error(traceback.format_exc())
                raise BLBadRequest("invalid_schema")
        elif data["status"] == VERIFICATION_STATUS_REJECTED:
            try:
                validate(data, admin_user_patch_unverified_schema)
                verified = False
            except ValidationError as e:
                app.logger.error(traceback.format_exc())
                raise BLBadRequest("invalid_schema")
        else:
            raise BLBadRequest("invalid_verification_status")

        user = get_user_by_id(user_id)
        if not user:
            raise BLNotFound("user_not_found")

        verification = Verification.objects(
            user=user,
            verification_type=VERIFICATION_SELFIE,
        ).first()
        if verification is None:
            app.logger.error(f"verification document not found for user {user.id}")
            raise BLBadRequest("selfie_document_not_found")

        if verification.status != VERIFICATION_STATUS_WAITING:
            app.logger.error(f"verification document is not in {VERIFICATION_STATUS_WAITING} step")
            raise BLBadRequest(f"selfie_document_already_{verification.status}")

        verification.verified_by = get_current_user()
        verification.verified_at = datetime.utcnow()
        verification.status = data["status"]
        if not verified:
            verification.data.update({
                "rejection_reason": data["rejection_reason"]
            })
            user.verifications.selfie = VERIFICATION_STATUS_REJECTED
            verification.save()
            user.save()
            return self.api_response(response_code=204)

        if "dob" in data:
            dob = data.pop("dob")
            user.dob = DateOfBirthItem(
                year=dob["year"],
                month=dob["month"],
                day=dob["day"]
            )

        national_code = data["national_code"]
        national_code_reverse = national_code[::-1]
        sum = 0
        for i in range(1, len(national_code_reverse)):
            sum += (i + 1) * int(national_code_reverse[i])

        control_character_modulus = sum % 11
        if control_character_modulus < 2:
            last_national_code_digit = control_character_modulus
        else:
            last_national_code_digit = 11 - control_character_modulus

        if not national_code == f"{national_code[:-1]}{last_national_code_digit}":
            raise BLBadRequest("invalid_national_code")

        for key in data:
            if key != "status":
                setattr(user, key, data[key])

        user.verifications = VerificationItem(
            selfie=VERIFICATION_STATUS_VERIFIED,
            email=user.verifications.email,
            bank_card=user.verifications.bank_card,
            phone=user.verifications.phone,
        )
        if user.verifications.bank_card == VERIFICATION_STATUS_VERIFIED \
                and user.verifications.phone == VERIFICATION_STATUS_VERIFIED:
            user.status = USER_ACTIVE

        user.save()
        verification.save()

        return self.api_response(response_code=204)
