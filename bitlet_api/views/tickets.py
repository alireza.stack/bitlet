from ..constants import USER_TYPES_CUSTOMER, TICKET_STATUS_WAITING_FOR_REPLY, \
    USER_TYPES_AGENT, TICKET_STATUS_REPLIED, TICKET_STATUS_CLOSED
from ..authorization import jwt_authenticate, get_current_user
from ..models import Ticket, Order, Department, TicketReplies
from ..schemas import add_ticket_schema, add_ticket_reply_schema, \
    patch_admin_ticket_schema, patch_ticket_schema
from flask import current_app as app, request, json
from jsonschema import validate, ValidationError
from flask_jwt_extended import get_jwt_identity
from ..errors import BLNotFound, BLBadRequest
from flask_restful import Resource
from ..api_mixin import BLAPIMixin
from ..workers import send_slack
from ..pipelines import Pipeline
from datetime import datetime
from ..utils import get_json
from bson import ObjectId
import traceback


class TicketRepliesView(Resource, BLAPIMixin):
    @jwt_authenticate()
    def post(self, ticket_id):
        try:
            data = request.get_json(force=True)
            validate(data, add_ticket_reply_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        user = get_current_user()
        ticket = Ticket.objects(
            id=ObjectId(ticket_id),
            user=user,
        ).first()

        if ticket is None:
            raise BLNotFound("ticket_not_found")

        ticket_reply = TicketReplies(
            user=user,
            user_type=USER_TYPES_CUSTOMER,
            ticket=ticket,
            **data
        )
        ticket_reply.save()

        ticket.status = TICKET_STATUS_WAITING_FOR_REPLY
        ticket.save()

        slack_cfg = json.loads(app.config["SLACK"])
        send_slack.delay(
            message=f":large_green_circle: user "
                    f"{user.first_name}-{user.last_name} with phone {user.id} "
                    f"replied to his ticket with id {ticket.id}",
            channel=slack_cfg["customer_support_channel"]
        )

        return self.api_response(response_code=204)


class TicketView(Resource, BLAPIMixin):
    @jwt_authenticate()
    def patch(self, ticket_id):
        try:
            data = request.get_json(force=True)
            validate(data, patch_ticket_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        ticket = Ticket.objects(
            id=ticket_id,
            user=ObjectId(get_jwt_identity())
        ).first()
        if ticket is None:
            raise BLNotFound("ticket_not_found")

        if ticket.status == TICKET_STATUS_CLOSED:
            app.logger.warning(f"ticket {ticket_id} is already closed with rate {ticket.rate}"
                               f" and cannot be updated with payload {data}")
            return self.api_response(response_code=204)

        ticket.rate = data["close"]["rate"]
        ticket.status = TICKET_STATUS_CLOSED
        ticket.save()

        return self.api_response(response_code=204)

    @jwt_authenticate()
    def get(self, ticket_id):
        user_id = get_jwt_identity()
        ticket = _get_ticket(dict(
            id=ObjectId(ticket_id),
            user=ObjectId(user_id)
        ))
        return self.api_response(content=ticket)


class TicketsView(Resource, BLAPIMixin):
    @jwt_authenticate()
    def get(self):
        user_id = get_jwt_identity()
        tickets_qset = Ticket.objects(user=ObjectId(user_id)).order_by('-_id').all()
        tickets = []
        for ticket in tickets_qset:
            ticket.department_info = get_json(ticket.department)
            ticket = get_json(ticket, ["department_id", "user_id", "is_locked", "locked_period"])
            tickets.append(ticket)

        return self.api_response(content=tickets)

    @jwt_authenticate()
    def post(self):
        try:
            data = request.get_json(force=True)
            validate(data, add_ticket_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        slack_cfg = json.loads(app.config["SLACK"])
        user = get_current_user()
        ticket = Ticket.objects(
            user=user,
            status__in=[
                TICKET_STATUS_WAITING_FOR_REPLY,
                TICKET_STATUS_REPLIED
            ]
        ).count()
        if ticket >= 2:
            app.logger.error("user has 2 pending tickets and trying to open more tickets!")
            send_slack.delay(
                message=f":x: user {user.first_name} {user.last_name} with phone {user.phone} "
                        f"is trying to open more support tickets, please call this "
                        f"user or reply his ticket with highest priority!\n "
                        f"Ticket: {data['title']}\nContent: {data['body']}",
                channel=slack_cfg["customer_support_channel"]
            )
            raise BLBadRequest("ticket_limit_exceeded")

        order = None
        if "order_id" in data:
            order = Order.objects(
                id=ObjectId(data["order_id"]),
                user=user
            ).first()

        department = Department.objects(id=ObjectId(data["department_id"])).first()
        if department is None:
            raise BLNotFound("department_not_found")

        ticket = Ticket(
            title=data["title"],
            order=order,
            department=department,
            user=user,
        )
        if "priority" in data:
            ticket.priority = data["priority"]

        ticket.save()

        ticket_reply = TicketReplies(
            user=user,
            user_type=USER_TYPES_CUSTOMER,
            body=data["body"],
            ticket=ticket,
            attachment=data.get("attachment"),
        )
        ticket_reply.save()

        send_slack.delay(
            message=f":large_green_circle: new ticket has been created by user "
                    f"{user.first_name}-{user.last_name} with id {user.id}",
            channel=slack_cfg["customer_support_channel"]
        )

        return self.api_response(response_code=204)


class AdminTicketView(Resource, BLAPIMixin):
    @jwt_authenticate("tickets")
    def patch(self, ticket_id):
        try:
            data = request.get_json(force=True)
            validate(data, patch_admin_ticket_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        user = get_current_user()
        ticket = Ticket.objects(
            id=ObjectId(ticket_id),
        ).first()

        if ticket is None:
            raise BLNotFound("ticket_not_found")

        ticket.locked_by = user
        ticket.locked_at = datetime.utcnow()
        ticket.is_locked = True
        ticket.save()

        return self.api_response(response_code=204)

    @jwt_authenticate("tickets")
    def post(self, ticket_id):
        try:
            data = request.get_json(force=True)
            validate(data, add_ticket_reply_schema)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")

        user = get_current_user()
        ticket = Ticket.objects(
            id=ObjectId(ticket_id),
        ).first()

        if ticket is None:
            raise BLNotFound("ticket_not_found")

        ticket_reply = TicketReplies(
            user=user,
            user_type=USER_TYPES_AGENT,
            ticket=ticket,
            **data
        )
        ticket_reply.save()

        ticket.status = TICKET_STATUS_REPLIED
        ticket.locked_by = None
        ticket.locked_at = None
        ticket.is_locked = False
        ticket.save()

        return self.api_response(response_code=204)

    @jwt_authenticate("tickets")
    def get(self, ticket_id):
        ticket = _get_ticket(dict(
            id=ObjectId(ticket_id)
        ))

        app.logger.info(f"returning ticket: {ticket}")
        return self.api_response(ticket)


class AdminTicketsView(Resource, BLAPIMixin):
    @jwt_authenticate("tickets")
    def get(self):
        app.logger.debug("getting all user tickets...")
        query = self.query
        app.logger.info(f"user provided query: {query}")
        pipeline = []
        Pipeline.search_tickets(pipeline=pipeline, query=query)

        total_pipe = pipeline + [{"$count": "total"}]
        total_pipe = list(Ticket.objects.aggregate(*total_pipe))
        try:
            total_pipe = next(iter(total_pipe))
        except StopIteration:
            return self.api_response(
                content=[],
                meta={
                    "total": 0
                }
            )

        pipeline += self.pagination_pipeline

        app.logger.info(f"final pipeline for tickets: {pipeline}")
        tickets = list(Ticket.objects.aggregate(*pipeline))
        tickets = [get_json(ticket) for ticket in tickets]

        return self.api_response(
            content=tickets,
            meta={
                "total": total_pipe["total"]
            }
        )


def _get_ticket(filters):
    app.logger.debug(f"getting ticket with filters {filters}...")
    ticket = Ticket.objects(
        **filters
    ).first()
    if ticket is None:
        raise BLNotFound("ticket_not_found")

    ticket_replies_qset = TicketReplies.objects(ticket=ticket).order_by('_id').all()
    ticket_replies = []
    for ticket_reply in ticket_replies_qset:
        ticket_reply.user_info = get_json(
            mongo_doc=ticket_reply.user,
            excludes=[
                "password",
                "verifications",
                "phone",
                "roles",
                "dob",
                "national_code"
            ]
        )
        ticket_reply = get_json(ticket_reply, ["user_id", "ticket_id"])
        ticket_replies.append(ticket_reply)

    ticket.department_info = get_json(ticket.department, ["department_id"])
    ticket.replies = ticket_replies

    return get_json(ticket)
