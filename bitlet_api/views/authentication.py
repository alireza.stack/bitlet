from flask_jwt_extended import jwt_required, get_jwt_identity, create_access_token
from ..utils import get_user_by_phone, get_tokens, validate_user_status
from ..schemas import login_by_code_schema, login_by_password_schema
from ..constants import REDIS_SMS_CODE, VERIFICATION_STATUS_VERIFIED
from jsonschema import validate, ValidationError
from flask import current_app as app, request
from mongoengine import DoesNotExist
from flask_restful import Resource
from ..api_mixin import BLAPIMixin
from ..errors import BLBadRequest
from datetime import datetime
from ..models import User
import phonenumbers
import traceback
import bcrypt


class TokenByPasswordView(Resource, BLAPIMixin):
    def post(self):
        try:
            data = request.get_json(force=True)
            validate(data, login_by_password_schema)
            ir_phone = phonenumbers.parse(data["phone"], "IR")
            phone = phonenumbers.format_number(ir_phone, phonenumbers.PhoneNumberFormat.E164)
            user = User.objects.get(phone=phone)
            user_id = str(user.id)
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")
        except phonenumbers.NumberParseException:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_phone")
        except DoesNotExist:
            app.logger.error(f"Login failed!\r\nphone number {data['phone']} does not exist!")
            raise BLBadRequest("authentication_failed")

        # it raises an error in case of an invalid user status
        validate_user_status(user)

        if not user.password:
            raise BLBadRequest("user_has_no_password")

        if bcrypt.hashpw(password=data["password"].encode("utf8"),
                         salt=user.password.encode("utf8")) != user.password.encode("utf8"):
            raise BLBadRequest("password_mismatch")

        access_token, refresh_token = get_tokens(user_id)

        return self.api_response(
            content={
                "access_token": access_token,
                "refresh_token": refresh_token
            }
        )


class TokenByCodeView(Resource, BLAPIMixin):
    def __init__(self):
        self.redis = app.extensions["redis"]

    def post(self):
        try:
            data = request.get_json(force=True)
            validate(data, login_by_code_schema)
            sms_code = data['code']
            user, user_id = get_user_by_phone(data["phone"])
        except ValidationError as e:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_schema")
        except phonenumbers.NumberParseException:
            app.logger.error(traceback.format_exc())
            raise BLBadRequest("invalid_phone")
        except DoesNotExist:
            app.logger.error(f"Login failed!\r\nphone number {data['phone']} does not exist!")
            raise BLBadRequest("authentication_failed")

        validate_user_status(user)

        user_login_code = f"{REDIS_SMS_CODE}:{user_id}"
        activation_code = self.redis.get(user_login_code)
        if not activation_code or sms_code != int(activation_code):
            app.logger.error(f"user {user.phone} has entered an invalid sms code for login!")
            raise BLBadRequest("invalid_sms_code")

        # if user's phone is not already verified
        user.verifications.phone = VERIFICATION_STATUS_VERIFIED
        user.last_login = datetime.utcnow()
        user.save()

        # invalidate SMS activation code
        self.redis.delete(user_login_code)
        app.logger.info(f"void SMS code {user_login_code} for user {user.phone}")

        access_token, refresh_token = get_tokens(user_id)

        return self.api_response(
            content={
                "access_token": access_token,
                "refresh_token": refresh_token
            }
        )


class RefreshView(Resource, BLAPIMixin):
    @jwt_required(refresh=True)
    def post(self):
        identity = get_jwt_identity()
        access_token = create_access_token(identity=identity)
        return self.api_response(
            content={
                "access_token": access_token
            })
