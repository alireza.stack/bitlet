"""Setup tools packaging information."""

from setuptools import setup, find_packages
import os

here = os.path.abspath(os.path.dirname(__file__))

# create a requirements.txt file using above packages
with open(os.path.join(here, "requirements.txt")) as fp:
    requires = [package.replace("\n", "").strip() for package in list(fp)]

setup(
    name="bitlet_api",
    version="20210905",
    description="Bitlet API",
    long_description="Frontier API is used to authenticate user either "
                     "internal or external and manage orders, payments, users",
    classifiers=[
        "Programming Language :: Python",
        "Framework :: Flask",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
    ],
    author="Seyyed Alireza Hoseini",
    author_email="alireza.stack@gmail.com",
    packages=find_packages(),
    include_package_data=True,
    package_data={'': ['uwsgi.ini']},
    zip_safe=False,
    install_requires=requires,
)
